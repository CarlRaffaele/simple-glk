GLK
===
This is a Java implementation of the GLK library for playing Inform games. It currently only supports text 
windows; however, the entire library implementation is planned. This library is planned to be cross-platform as much
 as possible.

To use, a virtual machine is required such as [jluxe](https://bitbucket.org/CarlRaffaele/jluxe/). This library is
 just for displaying the game data. 