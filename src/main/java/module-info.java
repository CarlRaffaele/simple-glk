module glk {
	opens glk to javafx.graphics, javafx.fxml;
	opens glk.objects.window to javafx.web;
	requires com.ibm.icu;
	requires BlorbReader;
	requires javafx.graphics;
	requires javafx.controls;
	requires javafx.web;
	requires jdk.xml.dom;
	requires Jluxe;
	requires javafx.fxml;
	requires jdk.jsobject;
	requires java.desktop;
	requires org.apache.logging.log4j;
	requires GameMemory;
	requires static org.jetbrains.annotations;
	requires static com.github.spotbugs.annotations;
}