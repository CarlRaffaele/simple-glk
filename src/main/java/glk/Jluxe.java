/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk;

import blorbReader.BlorbReader;
import blorbReader.UlxReader;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.MemorySegment;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import jluxe.executor.Globals;
import jluxe.executor.VMFunctions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

public class Jluxe extends Application {
	public MenuBar menu;
	public Stage mainStage;
	public BorderPane mainBorderPane;
	private static final Logger LOG = LogManager.getLogger(Jluxe.class);
	public CheckMenuItem MenuLiveUpdate;

	/**
	 * Open a blorb file and start the execution
	 *
	 * @param args Command line args that are currently ignored
	 */
	public static void main(@NotNull String[] args) {
		launch(args);
	}

	@Override
	@SuppressFBWarnings(justification = "Opening a game file can be done from anywhere.", value = {"PATH_TRAVERSAL_IN"})
	public void start(@NotNull Stage primaryStage) throws IOException {
		Stage root = FXMLLoader.load(Jluxe.class.getResource("/glk-resources/MainWindow.fxml"));
		root.show();
		if (getParameters().getRaw().size() > 0) {
			startGame(new File(getParameters().getRaw().get(0)));
		}
	}

	private static void startGame(File file) {
		Thread t = new Thread(() -> startFromPath(file.toPath()));
		t.setDaemon(true);
		t.setName("Game Thread");
		t.start();
	}

	@SuppressFBWarnings("CRLF_INJECTION_LOGS")
	public static void startFromPath(@NotNull Path inFile) {
		LOG.warn("Starting game: " + inFile.toString());
		BlorbReader blorb;
		UlxReader game;
		String file = inFile.toAbsolutePath().toString();
		try {
			switch (file.substring(file.lastIndexOf('.') + 1)) {
				case "gblorb" -> {
					blorb = BlorbReader.fromFile(inFile);
					game = new UlxReader(blorb.getExec(0).orElseThrow(() ->
							new IllegalArgumentException("Unable to find execution portion of blorb")));
				}
				case "ulx" -> {
					blorb = null;
					FileChannel fileChannel = FileChannel.open(inFile);
					game = new UlxReader(MemorySegment.fromBuffer(fileChannel.map(FileChannel.MapMode.READ_ONLY, 0,
							fileChannel.size())));
				}
				default -> throw new IllegalArgumentException("Unknown file type in file: " + file);
			}
		} catch (IOException e) {
			System.err.println("Unable to open file");
			e.printStackTrace();
			return;
		}

		VMFunctions.start(game.getGameData(), game.getGluxHeader(), game.getGluxDebuggingHeader(), blorb);
	}

	@FXML
	@SuppressFBWarnings(justification = "Can't be fixed until static design is fixed", value = {"ST_WRITE_TO_STATIC_FROM_INSTANCE_METHOD"})
	public void initialize() {
		Globals.glk = new GLK(mainBorderPane);
	}

	@SuppressWarnings({"unused", "MethodMayBeStatic"})
	public void close(ActionEvent ignored) {
		Platform.exit();
	}

	@SuppressWarnings("unused")
	public void open(ActionEvent ignored) {
		FileChooser fc = new FileChooser();
		fc.setTitle("Choose a game file");
		File file = fc.showOpenDialog(this.mainStage);
		if (file != null) {
			startGame(file);
		}
	}

	@SuppressWarnings("unused")
	public void toggleGraphicsDraw(ActionEvent ignored) {
		Globals.glk.setEnableLiveDrawing(MenuLiveUpdate.isSelected());
	}
}
