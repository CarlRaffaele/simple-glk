/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk;

import blorbReader.BlorbReader;
import blorbReader.QuetzalByteWriter;
import blorbReader.QuetzalFileReader;
import com.ibm.icu.lang.UCharacter;
import com.ibm.icu.text.BreakIterator;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.GameMemory;
import gameMemory.MemorySegment;
import glk.objects.*;
import glk.objects.stream.*;
import glk.objects.window.*;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jluxe.executor.RestorePackage;
import jluxe.executor.jluxeToGLK;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.text.Normalizer;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static blorbReader.QuetzalChunkType.*;

public class GLK implements jluxeToGLK {
	private static final int NULL = 0;
	@SuppressFBWarnings(justification = "Inherent weakness of static technique", value = {"MS_CANNOT_BE_FINAL"})
	public static GameMemory memory;
	private static boolean enableLiveDrawing = false;
	private final NavigableMap<Integer, Stream> streams;
	private final NavigableMap<Integer, Window> windows;
	private final NavigableMap<Integer, FileReference> filerefs;
	private final Map<WindowTypes, Map<Style, Map<StyleHint, Integer>>> windowStyles;
	private final LinkedBlockingQueue<event_t> events;
	private final BorderPane mainBorderPane;
	private BlorbReader file;
	Window currentWindow;
	private Window rootWindow;
	private WritableStream currentStream = null;
	private long timerStartMillis;
	private int timerDuration;

	public GLK(BorderPane mainBorderPane) {
		this.mainBorderPane = mainBorderPane;
		streams = new TreeMap<>();
		windows = new TreeMap<>();
		filerefs = new TreeMap<>();
		events = new LinkedBlockingQueue<>();
		windowStyles = new EnumMap<>(WindowTypes.class);
		for (WindowTypes wt : WindowTypes.values()) {
			windowStyles.put(wt, new EnumMap<>(Style.class));
			for (Style s : Style.values()) {
				windowStyles.get(wt).put(s, new EnumMap<>(StyleHint.class));
			}
		}
		windowStyles.remove(WindowTypes.ALL);
		mainBorderPane.heightProperty().addListener((ignored) -> events.add(new event_t(EventType.Arrange, null, 0,
				0)));
		mainBorderPane.widthProperty().addListener((ignored) -> events.add(new event_t(EventType.Arrange, null, 0,
				0)));
		timerStartMillis = 0;
		file = null;
	}

	@NotNull
	public static String stringFromAddress(int address) {
		StringBuilder ret = new StringBuilder();
		switch (memory.getByte(address)) {
			// C-Style String
			case (byte) 0xE0 -> {
				for (int i = address + 1, next = memory.getByte(i); next != 0; next = memory.getByte(++i)) {
					ret.appendCodePoint(next & 0xff);
				}
				return ret.toString();
			}
			// Unicode String
			case (byte) 0xE2 -> {
				for (int i = address + 3, next = memory.getInt(i); next != 0; next = memory.getInt(++i)) {
					ret.appendCodePoint(next);
				}
				return ret.toString();
			}
			default -> throw new IllegalArgumentException("Illegal string given to GLK to parse");
		}
	}

	public void setEnableLiveDrawing(boolean enableLiveDrawing) {
		windows.values()
			   .stream()
			   .filter(window -> window instanceof GraphicsWindow)
			   .forEach(window -> ((GraphicsWindow) window).setLiveDraw(enableLiveDrawing));
		GLK.enableLiveDrawing = enableLiveDrawing;
	}

	public static boolean isEnableLiveDrawing() {
		return enableLiveDrawing;
	}

	public int glk(int opcode, int[] args) {
		switch (opcode) {
			case 0x0001: // glk_exit
				Platform.exit();
				return NULL; // Shouldn't be reachable
			case 0x0002: // glk_set_interrupt_handler
				//Handled in VMFunctions, shouldn't be called.
				return NULL;
			case 0x0003: // glk_tick
				return NULL; // does nothing
			case 0x0004: // gestalt
			case 0x0005: // glk_gestalt_ext
				return gestalt(args);
			case 0x0020: // glk_window_iterate
				return glk_opaque_iterate(args[0], args[1], windows);
			case 0x0021: // glk_window_get_rock
				return windows.get(derefArg(args[0])).getRock();
			case 0x0022: // glk_window_get_root
				return rootWindow == null ? NULL : rootWindow.hashCode();
			case 0x0023: // glk_window_open
				return glk_window_open(derefArg(args[0]), args[1], args[2], WindowTypes.get(args[3]), args[4]);
			case 0x0024: // glk_window_close
				return glk_window_close(args[0], args[1]);
			case 0x0025: // glk_window_get_size
				windows.get(args[0]).getSize(args[1], args[2]);
				return NULL;
			case 0x0026: { // glk_window_set_arrangement
				AtomicBoolean done = ((PairWindow) windows.get(derefArg(args[0]))).setArrangement(args[1], args[2],
						windows.get(derefArg(args[3])));
				while (!done.get()) {
					Thread.onSpinWait();
				}
				return NULL;
			}
			case 0x0027: // glk_window_get_arrangement
				((PairWindow) windows.get(derefArg(args[0]))).getArrangement(args[1], args[2], args[3]);
				return NULL;
			case 0x0028: // glk_window_get_type
				return windows.get(args[0]).getType().ordinal();
			case 0x0029: // glk_window_get_parent
				return windows.get(args[0]).getParent().hashCode();
			case 0x002A: // glk_window_clear
				windows.get(derefArg(args[0])).clear();
				return NULL;
			case 0x002B: // glk_window_move_cursor
				windows.get(derefArg(args[0])).setCursor(args[1], args[2]);
				return NULL;
			case 0x002C: // glk_window_get_stream
				return windows.get(args[0]).getStream().hashCode();
			case 0x002D: // glk_window_set_echo_stream
				windows.get(args[0]).setEchoStream(((WritableStream) streams.get(args[1])));
				return NULL;
			case 0x002E: // glk_window_get_echo_stream
				return windows.get(args[0]).getEchoStream().map(Objects::hashCode).orElse(NULL);
			case 0x002F: // glk_set_window
				currentWindow = windows.get(derefArg(args[0]));
				if (!Objects.isNull(currentWindow)) {
					currentStream = (WritableStream) currentWindow.getStream().orElse(null);
				}
				return NULL;
			case 0x0030: // glk_window_get_sibling
				return windows.get(args[0])
							  .getSibling()
							  .map(Window::hashCode)
							  .orElse(NULL);
			case 0x0040: // glk_stream_iterate
				return glk_opaque_iterate(derefArg(args[0]), args[1], streams);
			case 0x0041: // glk_stream_get_rock
				return streams.get(derefArg(args[0])).getRock();
			case 0x0042: // glk_stream_open_file
				return glk_stream_open_file(filerefs.get(derefArg(args[0])), FileMode.get(args[1]), args[2], false);
			case 0x0138: // glk_stream_open_file_uni
				return glk_stream_open_file(filerefs.get(derefArg(args[0])), FileMode.get(args[1]), args[2], true);
			case 0x0043: // glk_stream_open_memory
				return glk_stream_open_memory(false, args[0], args[1], args[2], args[3]);
			case 0x0139: // glk_stream_open_memory_uni
				return glk_stream_open_memory(true, args[0], args[1], args[2], args[3]);
			case 0x0044: {// glk_stream_close
				Stream stream = streams.get(derefArg(args[0]));
				stream.close().writeUniqueValue(args[1]);
				streams.remove(stream.hashCode());
				return NULL;
			}
			case 0x0045: // glk_stream_set_position
				streams.get(derefArg(args[0])).setPosition(args[1], args[2]);
				return NULL;
			case 0x0046: // glk_stream_get_position
				return (int) streams.get(derefArg(args[0])).getPosition();
			case 0x0047: // glk_stream_set_current
				currentStream = (WritableStream) streams.getOrDefault(derefArg(args[0]), null);
				return NULL;
			case 0x0048: // glk_stream_get_current
				return (currentStream == null) ? 0 : currentStream.hashCode();
			case 0x0049: // glk_stream_open_resource
				return glk_stream_open_resource(args[0], args[1], false).orElse(NULL);
			case 0x013A: // glk_stream_open_resource_uni
				return glk_stream_open_resource(args[0], args[1], true).orElse(NULL);
			case 0x0060: // glk_fileref_create_temp
				return glk_fileref_create_temp(args[0], args[1]);
			case 0x0061: // glk_fileref_create_by_name
				return glk_fileref_create_by_name(args[0], args[1], args[2]);
			case 0x0062: // glk_fileref_create_by_prompt
				return glk_fileref_create_by_prompt(args[0], args[2]);
			case 0x0063: // glk_fileref_destroy
				filerefs.remove(derefArg(args[0])).destroy();
				return NULL;
			case 0x0064: // glk_fileref_iterate
				return glk_opaque_iterate(derefArg(args[0]), args[1], filerefs);
			case 0x0065: // glk_fileref_get_rock
				return filerefs.get(derefArg(args[0])).getRock();
			case 0x0066: // glk_fileref_delete_file
				filerefs.get(derefArg(args[0])).delete_file();
				return NULL;
			case 0x0067: // glk_fileref_does_file_exist
				return filerefs.get(derefArg(args[0])).does_file_exist() ? 1 : 0;
			case 0x0068: // glk_fileref_create_from_fileref
				return glk_fileref_create_by_fileref(args[0], filerefs.get(derefArg(args[1])), args[2]);
			case 0x0080: // glk_put_char
				currentStream.writeChar((byte) args[0]);
				return NULL;
			case 0x0128: // glk_put_char_uni
				currentStream.writeCharUni(args[0]);
				return NULL;
			case 0x0081: // glk_put_char_stream
				((WritableStream) streams.get(derefArg(args[0]))).writeChar((byte) args[1]);
				return NULL;
			case 0x012B: // glk_put_char_stream_uni
				((WritableStream) streams.get(derefArg(args[0]))).writeCharUni(args[1]);
				return NULL;
			case 0x0082: // glk_put_string
				currentStream.writeStringFromAddress(derefArg(args[0]));
				return NULL;
			case 0x0129: // glk_put_string_uni
				currentStream.writeStringFromAddressUni(derefArg(args[0]));
				return NULL;
			case 0x0083: // glk_put_string_stream
				((WritableStream) streams.get(args[0])).writeStringFromAddress(derefArg(args[1]));
				return NULL;
			case 0x012C: // glk_put_string_stream_uni
				((WritableStream) streams.get(args[0])).writeStringFromAddressUni(derefArg(args[1]));
				return NULL;
			case 0x0084: // glk_put_buffer
				currentStream.writeBuffer(derefArg(args[0]), args[1]);
				return NULL;
			case 0x012A: // glk_put_buffer_uni
				currentStream.writeBufferUni(derefArg(args[0]), args[1]);
				return NULL;
			case 0x0085: // glk_put_buffer_stream
				((WritableStream) streams.get(derefArg(args[0]))).writeBuffer(derefArg(args[1]), args[2]);
				return NULL;
			case 0x0086: // glk_set_style
				if (currentStream instanceof StyleableStream ss) {
					ss.setStyle(Style.getStyle(args[0]));
				}
				return NULL;
			case 0x0087: // glk_set_style_stream
				((StyleableStream) streams.get(derefArg(args[0]))).setStyle(Style.getStyle(args[0]));
				return NULL;
			case 0x012D: // glk_put_buffer_stream_uni
				((WritableStream) streams.get(derefArg(args[0]))).writeBufferUni(derefArg(args[1]), args[2]);
				return NULL;
			case 0x0090: // glk_get_char_stream
				return ((ReadableStream) streams.get(derefArg(args[0]))).readChar();
			case 0x0130: // glk_get_char_stream_uni
				return ((ReadableStream) streams.get(derefArg(args[0]))).readCharUni();
			case 0x0091: // glk_get_line_stream
				return ((ReadableStream) streams.get(derefArg(args[0]))).readLineIntoBuffer(args[1], args[2]);
			case 0x0132: // glk_get_line_stream_uni
				return ((ReadableStream) streams.get(derefArg(args[0]))).readLineUniIntoBuffer(args[1], args[2]);
			case 0x0092: // glk_get_buffer_stream
				return ((ReadableStream) streams.get(derefArg(args[0]))).readIntoBuffer(args[1], args[2]);
			case 0x0131: // glk_get_buffer_stream_uni
				return ((ReadableStream) streams.get(derefArg(args[0]))).readIntoBufferUni(args[1], args[2]);
			case 0x00A0: // glk_char_to_lower
				return Character.toLowerCase(args[0]);
			case 0x00A1: // glk_char_to_upper
				return Character.toUpperCase(args[0]);
			case 0x00B0: // glk_stylehint_set
				glk_stylehint_set(WindowTypes.get(args[0]), Style.getStyle(args[1]), StyleHint.getStyleHint(args[2]),
						args[3]);
				return NULL;
			case 0x00B1: // glk_stylehint_clear
				glk_stylehint_clear(WindowTypes.get(args[0]), Style.getStyle(args[1]), StyleHint.getStyleHint(args[2]));
				return NULL;
			case 0x00C0: // glk_select
				return glk_select(args);
			case 0x00C1: // glk_select_poll
				return glk_select_poll(args);
			case 0x00D0: // glk_request_line_event
				windows.get(derefArg(args[0])).requestLineEvent(args[1], args[2], args[3]);
				return NULL;
			case 0x0141: // glk_request_line_event_uni
				windows.get(derefArg(args[0])).requestLineEventUni(args[1], args[2], args[3]);
				return NULL;
			case 0x00D1: // glk_cancel_line_event
				windows.get(derefArg(args[0])).cancelLineEvent(args[1]);
				return NULL;
			case 0x00D2: // glk_request_char_event
				windows.get(derefArg(args[0])).requestCharEvent();
				return NULL;
			case 0x0140: // glk_request_char_event_uni
				windows.get(derefArg(args[0])).requestCharEventUni();
				return NULL;
			case 0x00D3: // glk_cancel_char_event
				windows.get(derefArg(args[0])).cancelCharEvent();
				return NULL;
			case 0x00D4: // glk_request_mouse_event
				windows.get(derefArg(args[0])).requestMouseEvent();
				return NULL;
			case 0x00D5: // glk_cancel_mouse_event
				windows.get(derefArg(args[0])).cancelMouseEvent();
				return NULL;
			case 0x00D6: // glk_request_timer_events
				if (args[0] == 0) { // Turn off the timer
					timerStartMillis = 0;
				} else {
					timerStartMillis = System.currentTimeMillis();
					timerDuration = args[0];
				}
				return NULL;
			case 0x00E0: // glk_image_get_info
				return glk_image_get_info(args);
			case 0x00E1: // glk_image_draw
				return file.getPictureChunk(args[1])
						   .map(image -> windows.get(derefArg(args[0]))
												.imageDraw(image, args[2], args[3]))
						   .map(ret -> ret ? 1 : 0)
						   .orElse(NULL);
			case 0x00E2: // glk_image_draw_scaled
				return file.getPictureChunk(args[1])
						   .map(image -> windows.get(derefArg(args[0]))
												.imageDrawScaled(image, args[2], args[3], args[4], args[5]))
						   .map(ret -> ret ? 1 : 0)
						   .orElse(NULL);
			case 0x00E8: // glk_window_flow_break
				windows.get(derefArg(args[0])).insertFlowBreak();
				return NULL;
			case 0x00E9: // glk_window_erase_rect
				((GraphicsWindow) windows.get(derefArg(args[0]))).eraseRect(args[1], args[2], args[3], args[4]);
				return NULL;
			case 0x00EA: // glk_window_fill_rect
				((GraphicsWindow) windows.get(derefArg(args[0]))).fillRect(args[1], args[2], args[3], args[4], args[5]);
				return NULL;
			case 0x00EB: // glk_window_set_background_color
				((GraphicsWindow) windows.get(derefArg(args[0]))).setBackgroundColor(args[1]);
				return NULL;
			case 0x0100: // glk_set_hyperlink
				currentStream.setHyperlink(args[0]);
				return NULL;
			case 0x0101: // glk_set_hyperlink_stream
				streams.get(derefArg(args[0])).setHyperlink(args[1]);
				return NULL;
			case 0x0102: // glk_request_hyperlink_event
				windows.get(derefArg(args[0])).requestHyperlinkEvent();
				return NULL;
			case 0x0103: // glk_cancel_hyperlink_event
				windows.get(derefArg(args[0])).cancelHyperlinkEvent();
				return NULL;
			case 0x0120: // glk_buffer_to_lower_case_uni
				return uniBufferLower(args[0], args[1], args[2]);
			case 0x0121: // glk_buffer_to_upper_case_uni
				return uniBufferUpper(args[0], args[1], args[2]);
			case 0x0122: // glk_buffer_to_title_case_uni
				return uniBufferTitle(args[0], args[1], args[2], args[3] != 0);
			case 0x0123: // glk_buffer_canon_decompose_uni
				return uniBufferNormalize(args[0], args[1], args[2], Normalizer.Form.NFD);
			case 0x0124: // glk_buffer_canon_normalize_uni
				return uniBufferNormalize(args[0], args[1], args[2], Normalizer.Form.NFC);
			case 0x0150: // glk_set_echo_line_event
				windows.get(derefArg(args[0])).setEchoLineEvent(args[1]);
				return NULL;
			case 0x0151: // glk_set_terminators_line_event
				windows.get(derefArg(args[0]))
					   .setLineTerminators(memory.slice(args[1], args[2] * 4));
				return NULL;
			case 0x0160: // glk_current_time
				glktimeval_t.now().writeUniqueValue((derefArg(args[0])));
				return NULL;
			case 0x0161: // glk_current_simple_time
				return (int) (Instant.now().getEpochSecond() / args[0]);
			case 0x0168: // glk_time_to_date_utc
				glk_time_to_date(derefArg(args[0]), derefArg(args[1]), ZoneId.of("Z"));
				return NULL;
			case 0x0169: // glk_time_to_date_local
				glk_time_to_date(derefArg(args[0]), derefArg(args[1]), ZoneId.systemDefault());
				return NULL;
			case 0x016A: // glk_simple_time_to_date_utc
				glk_simple_time_to_date(args[0], args[1], derefArg(args[2]), ZoneId.of("Z"));
				return NULL;
			case 0x016B: // glk_simple_time_to_date_local
				glk_simple_time_to_date(args[0], args[1], derefArg(args[2]), ZoneId.systemDefault());
				return NULL;
			case 0x016C: // glk_date_to_time_utc
				glk_date_to_time(derefArg(args[0]), derefArg(args[1]), ZoneId.of("Z"));
				return NULL;
			case 0x016D: // glk_date_to_time_local
				glk_date_to_time(derefArg(args[0]), derefArg(args[1]), ZoneId.systemDefault());
				return NULL;
			case 0x016E: // glk_date_to_simple_time_utc
				try {
					return (int) Math.floor(new glkdate_t(derefArg(args[0])).toZonedDateTime(ZoneId.of("Z"))
																			.toEpochSecond() / ((double) args[1]));
				} catch (DateTimeException e) {
					return -1;
				}
			case 0x016F: // glk_date_to_simple_time_local
				try {
					return (int) Math.floor(new glkdate_t(derefArg(args[0])).toZonedDateTime(ZoneId.systemDefault())
																			.toEpochSecond() / ((double) args[1]));
				} catch (DateTimeException e) {
					return -1;
				}
				// ****************Style Opcodes****************//
			case 0x00B2: // glk_style_distinguish
			case 0x00B3: // glk_style_measure
				return NULL; // TODO: Implement
			// ****************Sound Opcodes****************//
			case 0x00F0: // glk_schannel_iterate
			case 0x00F1: // glk_schannel_get_rock
			case 0x00F2: // glk_schannel_create
			case 0x00F3: // glk_schannel_destroy
			case 0x00F4: // glk_schannel_create_ext
			case 0x00F7: // glk_schannel_play_multi
			case 0x00F8: // glk_schannel_play
			case 0x00F9: // glk_schannel_play_ext
			case 0x00FA: // glk_schannel_stop
			case 0x00FB: // glk_schannel_set_volume
			case 0x00FC: // glk_sound_load_hint
			case 0x00FD: // glk_schannel_set_volume_ext
			case 0x00FE: // glk_schannel_pause
			case 0x00FF: // glk_schannel_unpause
				throw new UnsupportedOperationException("Sound is not supported");
			default:
				throw new IllegalArgumentException("Unknown glk.glk function: 0x" + Integer.toHexString(args[0]));
		}
	}

	private int glk_image_get_info(int[] args) {
		if (file == null) {
			return 0;
		}
		Optional<Image> image = file.getPicture(args[0]);
		if (image.isEmpty()) {
			return 0;
		}
		Image img = image.get();
		if (args[1] != 0) {
			if (args[1] == -1) {
				memory.getCurrentCallFrame().push((int) img.getWidth());
			} else {
				memory.put(args[1], (int) img.getWidth());
			}
		}
		if (args[2] != 0) {
			if (args[2] == -1) {
				memory.getCurrentCallFrame().push((int) img.getHeight());
			} else {
				memory.put(args[2], (int) img.getHeight());
			}
		}
		return 1;
	}

	private int glk_select_poll(int[] args) {
		if (timerStartMillis != 0 && System.currentTimeMillis() > (timerStartMillis + timerDuration)) {
			timerStartMillis = System.currentTimeMillis();
			event_t.TIMER_EVENT.writeUniqueValue(args[0]);
		} else if (events.peek() != null && !events.peek().type().isPlayerInput()) {
			//This should still hold safe for poll because we know an event is available
			while (true) {
				try {
					events.take().writeUniqueValue(args[0]);
					break;
				} catch (InterruptedException ignored) {
				}
			}
		} else {
			event_t.NULL_EVENT.writeUniqueValue(args[0]);
		}
		return NULL;
	}

	private int glk_select(int[] args) {
		if (!enableLiveDrawing) {
			Platform.runLater(() -> windows.values()
										   .stream()
										   .filter(window -> window instanceof GraphicsWindow)
										   .forEach(window -> ((GraphicsWindow) window).draw()));
		}
		while (true) {
			try {
				if (timerStartMillis != 0) {
					event_t ret = events.poll(timerDuration, TimeUnit.MILLISECONDS);
					Objects.requireNonNullElse(ret, event_t.TIMER_EVENT).writeUniqueValue(args[0]);
				} else {
					events.take().writeUniqueValue(args[0]);
				}
				break;
			} catch (InterruptedException ignored) {
			}
		}
		return NULL;
	}

	private static int gestalt(int[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i] == -1) {
				args[i] = memory.getCurrentCallFrame().pop();
			}
		}
		switch (args[0]) {
			case 0: //Version
				return 0x00000705; //Version of GLK spec this implements
			case 1: //Char Input
				//Fall through
			case 2: //Line Input
				return Character.isISOControl(args[1]) ? 0 : 1; //player can't type control characters
			case 3: //Char Output
				//Tests if a Unicode value can be printed. Java supports all...hopefully
				if (args.length > 2 && args[1] != 0) {
					memory.put(args[2], 1);
				}
				return Character.isISOControl(args[1]) ? 0 : 2; //gestalt_CharOutput_CannotPrint or
			//gestalt_CharOutput_ExactPrint
			case 4: //Mouse Input
				return 1;
			case 5: //Timer
				return 1;
			case 6: //Graphics
				return 1;
			case 7: //Draw Image
				return WindowTypes.get(args[1]).supportsImages ? 1 : 0;
			case 8: //Sound
				return 0;
			case 9: //Sound Volume
				return 0;
			case 10: //Sound Notify
				return 0;
			case 11: //Hyperlinks
				return 1;
			case 12: //Hyperlink Input
				return WindowTypes.get(args[1]).supportsHyperlinks ? 1 : 0;
			case 13: //Sound Music
				return 0;
			case 14: //Graphics Transparency
				return 1;
			case 15: //Unicode
				return 1;
			case 16: //Unicode Norm
				return 1;
			case 17: //Line Input Echo
				return 1;
			case 18: //Line Terminators
				return 1;
			case 19: //Line Terminator Key
				return 1;
			case 20: //Date Time
				return 1;
			case 21: //Sound2
				return 0;
			case 22: //Resource Stream
				return 1;
			case 23: //Graphics Char Input
				return 0;
			default:
				System.err.println("WARNING: unknown glk gestalt value: " + Arrays.toString(args));
				return 0;
		}
	}

	/**
	 * Iterates through the streams to find the "next" stream given a stream. Calling glk_stream_iterate(NULL, r)
	 * returns the first object; calling glk_stream_iterate(obj, r) returns the next object, until there aren't any
	 * more, at which time it returns NULL. Instead of NULL a value of 0 is used.
	 *
	 * @param stream     The stream to find the next stream
	 * @param rock       The rock location to store the stream's rock value
	 * @param collection The collection to iterate over
	 * @return The next stream
	 */
	private static <T extends GLKOpaqueObject> int glk_opaque_iterate(int stream, int rock,
																	  @NotNull NavigableMap<Integer, T> collection) {

		Map.Entry<Integer, T> next = stream == 0 ? collection.firstEntry() : collection.ceilingEntry(stream + 1);
		if (next != null && next.getKey() == stream) {
			next = null;
		}
		if (next != null) {
			if (rock > 0) {
				memory.put(rock, next.getValue().getRock());
			} else if (rock == -1) {
				memory.getCurrentCallFrame().push(next.getValue().getRock());
			}
			return next.getValue().hashCode();
		}

		if (rock > 0) {
			memory.put(rock, 0);
		} else if (rock == -1) {
			memory.getCurrentCallFrame().push(0);
		}
		return NULL;
	}

	private static int derefArg(int arg) {
		return arg == -1 ? memory.getCurrentCallFrame().pop() : arg;
	}

	private int glk_window_open(int split, int method, int size, WindowTypes wintype, int rock) {
		Window w = Window.windowFrom(wintype, rock, this, windowStyles.get(wintype));
		insertOpaqueCollection(windows, w);
		while (!w.isDoneLoading()) { //block until webviews have finished loading
			Thread.onSpinWait();
		}
		insertOpaqueCollection(streams, w.getStream().orElseThrow());
		if (rootWindow == null) {
			Platform.runLater(() -> mainBorderPane.setCenter(w.getNode()));
			rootWindow = w;
		} else {
			Window existing = windows.get(split);
			if (existing == null) {
				windows.remove(w.hashCode());
				streams.remove(w.getStream().hashCode());
				w.close(0);
				throw new IllegalArgumentException("Tried to split a non-existent Window: " + split);
			}
			PairWindow pair = Window.split(existing, w, MethodDirection.get(method));
			insertOpaqueCollection(windows, pair);
			if (existing.getNode() == mainBorderPane.getCenter()) {
				Platform.runLater(() -> mainBorderPane.setCenter(pair.getNode()));
			}
			if (existing == rootWindow) {
				rootWindow = pair;
			}
			AtomicBoolean done = pair.setArrangement(method, size, w);
			//block until the window is drawn.
			while (!done.get()) {
				Thread.onSpinWait();
			}
		}

		return w.hashCode();
	}

	private int glk_window_close(int windowID, int saveAddress) {
		Window w = windows.get(windowID);
		windows.remove(windowID);
		w.getStream().map(Stream::hashCode).ifPresent(streams::remove);
		if (w == rootWindow) {
			rootWindow = null;
		}
		w.close(saveAddress);
		if (w.getParent().flatMap(Window::getParent).isEmpty()) { //Sibling is becoming root
			rootWindow = w.getSibling().orElseThrow(); //Should have sibling
			rootWindow.setParent(null);
			//Reset the constraints
			rootWindow.setMaxHeight(Double.MAX_VALUE);
			rootWindow.setMaxWidth(Double.MAX_VALUE);
			VBox.clearConstraints(rootWindow.getNode());
			HBox.clearConstraints(rootWindow.getNode());
			//Add to the scene
			Platform.runLater(() -> mainBorderPane.setCenter(rootWindow.getNode()));
		}
		//Remove the Pair window
		w.getParent().ifPresent(window -> windows.remove(window.hashCode()));
		if (w.getStream().isPresent()) {
			//check if this window's stream was anybody's echo stream
			for (Window window : windows.values()) {
				if (window.getEchoStream().equals(w.getStream())) {
					window.setEchoStream(null);
					break;
				}
			}
		}
		return NULL;
	}

	private Optional<Integer> glk_stream_open_resource(int fileNum, int rock, boolean unicode) {
		Stream ns = Stream.resourceStreamFrom(fileNum, rock, unicode, file);
		if (ns == null) {
			return Optional.empty();
		}
		return Optional.of(insertOpaqueCollection(streams, ns));
	}

	private int glk_stream_open_file(FileReference fileRef, FileMode fmode, int rock, boolean unicode) {
		Stream ns = Stream.fileStreamFrom(fileRef, fmode, unicode, rock);
		return insertOpaqueCollection(streams, ns);
	}

	/**
	 * Opens a new memory stream
	 *
	 * @param unicode True if the stream is a unicode stream, false if it is an ASCII stream
	 * @param rock    The rock value of the stream
	 * @param fmode   The file mode of the stream
	 * @param length  The length in memory of the stream
	 * @param address The memory address to start the stream at
	 * @return The unique value representing the stream
	 */
	private int glk_stream_open_memory(boolean unicode, int address, int length, int fmode, int rock) {
		Stream ns = Stream.memoryStreamFrom(unicode, address, length, fmode, rock);
		return insertOpaqueCollection(streams, ns);
	}

	private int glk_fileref_create_temp(int usage, int rock) {
		return insertOpaqueCollection(filerefs, FileReference.CreateTemp(usage, rock));
	}

	private int glk_fileref_create_by_name(int usage, int nameAddr, int rock) {
		return insertOpaqueCollection(filerefs, FileReference.CreateByName(usage, nameAddr, rock));
	}

	private int glk_fileref_create_by_prompt(int usage, int rock) {
		FileRef f = FileReference.CreateByPrompt(usage, rock, mainBorderPane.getScene().getWindow());
		if (f == null) {
			return 0;
		}
		return insertOpaqueCollection(filerefs, f);
	}

	private int glk_fileref_create_by_fileref(int usage, FileReference fileRef, int rock) {
		return insertOpaqueCollection(filerefs, FileReference.CreateByFileref(usage, fileRef, rock));
	}

	private void glk_stylehint_set(WindowTypes windowTypes, Style style, StyleHint styleHint, int value) {
		if (windowTypes == WindowTypes.ALL) {
			for (int i = 1; i < WindowTypes.values().length; i++) { // Skip ALL
				windowStyles.get(WindowTypes.get(i)).get(style).put(styleHint, value);
			}
		} else {
			windowStyles.get(windowTypes).get(style).put(styleHint, value);
		}
	}

	private void glk_stylehint_clear(WindowTypes windowTypes, Style style, StyleHint styleHint) {
		if (windowTypes == WindowTypes.ALL) {
			for (int i = 1; i < WindowTypes.values().length; i++) { // Skip ALL
				windowStyles.get(WindowTypes.get(i)).get(style).remove(styleHint);
			}
		} else {
			windowStyles.get(windowTypes).get(style).remove(styleHint);
		}
	}

	private static int uniBufferLower(int addr, int len, int numChars) {
		String sret = UCharacter.toLowerCase(Locale.ROOT, stringFromBuffer(addr, numChars));
		int[] ret = sret.codePoints().limit(len).toArray();
		memory.put(addr, ret);
		return sret.length();
	}

	/**
	 * Unicode title case a buffer. This functions per:
	 * https://eblong.com/zarf/glk/Glk-Spec-075.html#encoding_hilo
	 *
	 * @param addr     The start address of the buffer
	 * @param len      The max length of the buffer
	 * @param numChars The number of characters in the buffer
	 * @return The length of the string after conversion
	 */
	private static int uniBufferUpper(int addr, int len, int numChars) {
		String rets = UCharacter.toUpperCase(Locale.ROOT, stringFromBuffer(addr, numChars));
		int[] codePoints = rets.codePoints().limit(len).toArray();
		memory.put(addr, codePoints);
		return rets.length();
	}

	private static int uniBufferTitle(int addr, int len, int numChars, boolean lowerRest) {
		String sret = stringFromBuffer(addr, numChars);
		if (lowerRest) {
			sret = UCharacter.toTitleCase(Locale.ROOT, sret, BreakIterator.getSentenceInstance());
		} else {
			sret = UCharacter.toTitleCase(Locale.ROOT, sret, BreakIterator.getSentenceInstance(), UCharacter.TITLECASE_NO_LOWERCASE);
		}
		int[] ret = sret.codePoints().limit(len).toArray();
		memory.put(addr, ret);
		return sret.length();
	}

	private static int uniBufferNormalize(int addr, int len, int numChars, Normalizer.Form form) {
		String sret = Normalizer.normalize(stringFromBuffer(addr, numChars), form);
		int[] ret = sret.codePoints().limit(len).toArray();
		memory.put(addr, ret);
		return sret.length();
	}

	private static void glk_time_to_date(int timePtr, int datePtr, ZoneId zoneId) {
		glktimeval_t time = new glktimeval_t(timePtr);
		Instant date = Instant.ofEpochSecond(time.seconds(), time.microsec() * 1000L);
		new glkdate_t(date.atZone(zoneId)).writeUniqueValue(datePtr);
	}

	private static void glk_simple_time_to_date(long time, int factor, int datePtr, ZoneId zoneId) {
		new glkdate_t(Instant.ofEpochSecond(time * factor).atZone(zoneId)).writeUniqueValue(datePtr);
	}

	private static void glk_date_to_time(int datePtr, int timePtr, ZoneId zoneId) {
		ZonedDateTime zonedDateTime;
		try {
			zonedDateTime = new glkdate_t(datePtr).toZonedDateTime(zoneId);
		} catch (DateTimeException e) {
			new glktimeval_t(-1, 0).writeUniqueValue(timePtr);
			return;
		}
		new glktimeval_t(zonedDateTime.toEpochSecond(), zonedDateTime.get(ChronoField.MICRO_OF_SECOND))
				.writeUniqueValue(timePtr);
	}

	private static <T extends GLKOpaqueObject> int insertOpaqueCollection(@NotNull NavigableMap<Integer, T> collection,
																		  @NotNull T value) {
		int hash = collection.size() > 0 ? collection.lastKey() + 1 : 1;
		value.setID(hash);
		collection.put(hash, value);
		return hash;
	}

	@NotNull
	public static String stringFromBuffer(int addr, int len) {
		if (Character.isValidCodePoint(memory.getInt(addr))) { //Assume Unicode buffer
			int[] ret = new int[len];
			memory.getInts(addr, ret);
			return new String(ret, 0, len);
		} else { //Assume Latin-1 buffer
			byte[] ret = new byte[len];
			memory.getBytes(addr, ret);
			return new String(ret, Charset.forName("Latin1"));
		}
	}

	public boolean saveGame(int saveStream, MemorySegment originalGame) {
		QuetzalByteWriter writer = new QuetzalByteWriter();
		writer.addChunk(UNCOMPRESSED_MEMORY, memory.asReadOnly().position(0));
//		writer.addChunk(COMPRESSED_MEMORY, CMemHandler.compressSave(originalGame, Globals.gameMemory));
		writer.addChunk(IF_HEADER, memory.asReadOnly().slice(0, 128));
		writer.addChunk(STACKS, memory.getStack().slice(0, memory.getCurrentCallFrame().getStackPointer()));
		//TODO - Add heap save when implemented
		return ((FileStream) streams.get(saveStream)).writeBuffer(writer.toSegment());
	}

	@NotNull
	public RestorePackage restoreGame(int loadStream) {
		return new RestorePackage(new QuetzalFileReader(((FileStream) streams.get(loadStream)).getPath()));
	}

	public void writeChar(int c) {
		currentStream.writeCharUni(c);
	}

	public void writeInt(int i) {
		currentStream.writeInt(i);
	}

	public void writeString(String s) {
		currentStream.writeString(s);
	}

	public void notifyMemoryResize() {
		streams.values().forEach(Stream::notifyMemoryResize);
	}

	@SuppressFBWarnings(justification = "Can't be fixed until static design is fixed", value = {"ST_WRITE_TO_STATIC_FROM_INSTANCE_METHOD"})
	public void setGameMemory(GameMemory gameMemory) {
		memory = gameMemory;
	}

	@Override
	public void setGameBlorb(@Nullable BlorbReader blorb) {
		this.file = blorb;
	}

	public void insertEvent(event_t event) {
		events.add(event);
	}
}
