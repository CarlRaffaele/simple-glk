/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import glk.objects.stream_result_t;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

abstract class BaseFileStream extends BaseStream implements FileStream {
	final FileReference fileReference;

	BaseFileStream(@NotNull FileReference fileReference, FileMode fmode, int rock) {
		super(fmode, rock);
		this.fileReference = fileReference;
		fileReference.addUsage(this);
	}

	@Override
	public Path getPath() {
		return fileReference.getPath();
	}


	@Override
	public stream_result_t close() {
		fileReference.removeUsage(this);
		return super.close();
	}
}
