/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;

import java.nio.file.Path;

public interface FileStream extends Stream {
	boolean writeBuffer(byte[] bytes);

	boolean writeBuffer(MemorySegment buffer);

	Path getPath();

	class UnableToCreateFileException extends RuntimeException {
		UnableToCreateFileException(String message, Throwable cause) {
			super(message, cause);
		}
	}

	class IllegalFileModeException extends RuntimeException {
		IllegalFileModeException(String message) {
			super(message);
		}
	}

	class UnableToReadFileException extends RuntimeException {
		UnableToReadFileException(String message, Throwable cause) {
			super(message, cause);
		}
	}

	class UnableToWriteFileException extends RuntimeException {
		UnableToWriteFileException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}
