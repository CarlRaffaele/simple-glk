/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.objects.stream_result_t;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class ReadableWritableTextFileStream extends BaseFileStream implements ReadableStream, WritableStream {
	private final SeekableByteChannel file;
	private final ByteBuffer temp;

	ReadableWritableTextFileStream(FileReference fileReference, FileMode fmode, int rock) {
		super(fileReference, fmode, rock);
		try {
			file = Files.newByteChannel(fileReference.getPath(), fmode.getOpenOptions());
		} catch (IOException e) {
			throw new UnableToReadFileException("Unable to open file: " + fileReference, e);
		}

		temp = ByteBuffer.wrap(new byte[1]);
	}

	@Override
	public stream_result_t close() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error closing file stream.");
		}
		return super.close();
	}

	@Override
	public long getPosition() {
		try {
			return file.position();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error getting filestream position.");
			return -1;
		}
	}

	@Override
	public void setPosition(long position, int seekmode) {
		try {
			file.position(position);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error setting position of file.");
			return;
		}
		super.position = getPosition();
		super.setPosition(position, seekmode);
	}

	@Override
	long getEof() {
		try {
			return file.size();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error getting size of file.");
			return -1;
		}
	}

	@Override
	public boolean writeBuffer(byte[] bytes) {
		if (echo != null) {
			for (byte b : bytes) {
				echo.writeChar(b);
			}
		}
		try {
			file.write(ByteBuffer.wrap(bytes));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	@Override
	public byte readChar() {
		try {
			if (file.read(temp.rewind()) == -1) {
				return -1;
			}
			readCount++;
			return temp.get(0);
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public boolean writeBuffer(MemorySegment buffer) {
		throw new UnsupportedOperationException();
//		if (echo != null) {
//			buffer.mark();
//			while (buffer.hasRemaining()) {
//				echo.writeChar(buffer.getByte());
//			}
//			buffer.reset();
//		}
//		try {
//			file.write(buffer);
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
//		return true;
	}

//	@Override
//	public void writeBuffer(int addr, int len) {
//		if (echo != null) {
//			echo.writeBuffer(addr, len);
//		}
//		try {
//			file.write(GLK.memory.slice(addr, len));
//		} catch (IOException e) {
//			e.printStackTrace();
//			System.err.println("Unable to writeBuffer.");
//		}
//	}
//
//	@Override
//	public int readIntoBuffer(int addr, int len) {
//		int amountRead;
//		try {
//			amountRead = file.read(GLK.memory.slice(addr, len));
//		} catch (IOException e) {
//			e.printStackTrace();
//			System.err.println("Error reading values into memory from file.");
//			return -1;
//		}
//		if (amountRead >= len) {
//			amountRead--;
//		}
//		GLK.memory.put(addr + amountRead, (byte) 0);
//		readCount += amountRead;
//		return amountRead;
//	}
//
//	@Override
//	public int readLineIntoBuffer(int addr, int len) {
//		long save;
//		try {
//			save = file.position();
//		} catch (IOException e) {
//			e.printStackTrace();
//			return -1;
//		}
//		MemorySegment slice = GLK.memory.slice(addr, len);
//		try {
//			file.read(slice);
//		} catch (IOException e) {
//			e.printStackTrace();
//			return -1;
//		}
//		slice.limit(slice.position()).rewind();
//		//noinspection StatementWithEmptyBody,HardcodedLineSeparator
//		while (slice.hasRemaining() && slice.get() != '\n') {
//		}
//		int amountRead = slice.limit(slice.capacity()).position();
//		if (amountRead >= len) {
//			amountRead--;
//		}
//		try {
//			file.position(save + amountRead);
//		} catch (IOException e) {
//			e.printStackTrace();
//			return -1;
//		}
//		readCount += amountRead;
//		slice.put(amountRead, (byte) 0);
//		return amountRead;
//	}

	@Override
	public int readLineUniIntoBuffer(int addr, int len) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void writeChar(byte b) {
		if (echo != null) {
			echo.writeChar(b);
		}
		try {
			file.write(temp.rewind().put(0, b));
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Unable to writechar to file.");
			return;
		}
		writeCount++;
	}

	@Override
	public void writeString(String s) {
		if (echo != null) {
			echo.writeString(s);
		}
		byte[] bytes = s.getBytes(StandardCharsets.ISO_8859_1);
		try {
			file.write(ByteBuffer.wrap(bytes));
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Unable to writestring to file.");
			return;
		}
		writeCount += bytes.length;
	}
}
