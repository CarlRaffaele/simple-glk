/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import glk.GLK;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileRef implements FileReference {
	@NotNull private final FileUsage fileUsage;
	@NotNull private final Path path;
	@NotNull private final List<FileStream> users;
	private final boolean textMode; //False = binary mode
	private final boolean isTempReference;
	private boolean isDestroyed;
	private int uniqueValue;
	private final int rock;


	/**
	 * Create a File Reference object.
	 *
	 * @param usage           An int specifying how the file will be used
	 * @param rock            A rock value for the reference
	 * @param path            The path object
	 * @param isTempReference True if this references a temporary fileref, i.e. a file that should be deleted when
	 *                        the program is no longer using it.
	 */
	FileRef(int usage, int rock, @NotNull Path path, boolean isTempReference) {
		this.rock = rock;
		this.fileUsage = FileUsage.get(usage & 0b111);
		this.textMode = (usage & 0x100) != 0;
		this.path = path;
		this.isTempReference = isTempReference;
		this.isDestroyed = false;
		users = new ArrayList<>(3); //3 is arbitrary, but the average will likely be close to 1
	}

	@Override
	public void delete_file() {
		try {
			Files.deleteIfExists(this.path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean does_file_exist() {
		return Files.exists(this.path);
	}

	@Override
	public boolean isTextMode() {
		return textMode;
	}

	@Override
	public void destroy() {
		isDestroyed = true;
	}

	@Override
	public void addUsage(FileStream fileStream) {
		users.add(fileStream);
	}

	@Override
	public void removeUsage(FileStream fileStream) {
		users.remove(fileStream);
		if (isTempReference && isDestroyed && users.isEmpty()) {
			System.err.println("Deleting temp file: " + path);
			delete_file();
		}
	}

	@NotNull
	@Override
	public Path getPath() {
		return path;
	}

	@Override
	public int getRock() {
		return this.rock;
	}

	@Override
	public void setID(int id) {
		this.uniqueValue = id;
	}

	@Override
	public void writeUniqueValue(int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push(uniqueValue);
		} else {
			GLK.memory.put(address, uniqueValue);
		}
	}

	@Override
	public int hashCode() {
		return uniqueValue;
	}

	@Override
	public String toString() {
		return "FileRef{" +
				", fileUsage=" + fileUsage +
				", path=" + path +
				", textMode=" + textMode +
				", uniqueValue=" + uniqueValue +
				", rock=" + rock +
				'}';
	}

	@Override
	@SuppressFBWarnings(justification = "False positive", value = {"SA_LOCAL_SELF_COMPARISON"})
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof FileRef fileRef)) {
			return false;
		}
		return textMode == fileRef.textMode &&
				uniqueValue == fileRef.uniqueValue &&
				rock == fileRef.rock &&
				fileUsage == fileRef.fileUsage &&
				path.equals(fileRef.path);
	}
}
