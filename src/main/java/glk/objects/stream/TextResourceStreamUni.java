/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;

public class TextResourceStreamUni extends BaseStream implements ReadableStream {
	private final MemorySegment data;

	TextResourceStreamUni(int rock, @NotNull MemorySegment data) {
		super(FileMode.READ, rock);
		this.data = data;
		super.eof = data.length();
		super.size = data.length();
	}

	@Override
	public void setPosition(long position, int seekmode) {
		super.setPosition(position, seekmode);
		data.position((int) super.position);
	}

	@Override
	public int readCharUni() {
		//Adopted from ReadableWritableTextFileStreamUni
		if (!data.hasRemaining()) {
			return -1;
		}
		byte firstByte = data.getByte(data.position());
		//UTF-8 Continuation bytes
		readCount++;
		if ((firstByte & 0b10000000) == 0) {
			//Not UTF-8 and can skip the decoding step
			position++;
			return data.getByte() & 0xff;
		} else if ((firstByte & 32) == 0) {
			position += 2;
			return ((data.getByte() & 0x3F) << 6) | (data.getByte() & 0x3F);
		} else if ((firstByte & 16) == 0) {
			position += 3;
			return ((data.getByte() & 0x1F) << 12) | ((data.getByte() & 0x3F) << 6) | (data.getByte() & 0x3F);
		} else if ((firstByte & 8) == 0) {
			position += 4;
			return ((data.getByte() & 0xF) << 18)
					| ((data.getByte() & 0x3F) << 12)
					| ((data.getByte() & 0x3F) << 6)
					| (data.getByte() & 0x3F);
		} else {
			throw new IllegalArgumentException("Unknown UTF-8 Continuation byte: " + firstByte);
		}
	}
}
