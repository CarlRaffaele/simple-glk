/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.MemorySegment;
import glk.GLK;
import glk.objects.stream_result_t;
import org.jetbrains.annotations.NotNull;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

class ReadableWritableBinaryFileStreamUni extends BaseFileStream implements ReadableStream, WritableStream {
	private final RandomAccessFile file;

	//TODO: Find a way to limit the files that the game can open
	@SuppressFBWarnings(justification = "Possible security threat, but requires a rewrite of how file paths are " +
			"handled. The switch is intentional.", value = {"PATH_TRAVERSAL_IN", "SF_SWITCH_NO_DEFAULT"})
	ReadableWritableBinaryFileStreamUni(@NotNull FileReference fileRef, FileMode fileMode, int rock) {
		super(fileRef, fileMode, rock);

		try {
			this.file = new RandomAccessFile(fileRef.getPath().toString(), fileMode.getFilesystemMode());
		} catch (FileNotFoundException e) {
			throw new UnableToCreateFileException("Unable to find file: " + fileRef.getPath(), e);
		}

		try {
			switch (fileMode) {
				case WRITE -> file.setLength(0);
				case WRITE_APPEND -> file.seek(file.length());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public stream_result_t close() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return super.close();
	}

	@Override
	public long getPosition() {
		try {
			return file.getFilePointer();
		} catch (IOException e) {
			return -1;
		}
	}

	@Override
	long getEof() {
		try {
			return file.length();
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public void setPosition(long position, int seekmode) {
		super.position = getPosition();
		super.setPosition(position, seekmode);
		try {
			file.seek(super.position);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean writeBuffer(byte[] bytes) {
		if (echo != null) {
			for (byte b : bytes) {
				echo.writeChar(b);
			}
		}
		try {
			file.write(bytes);
		} catch (IOException e) {
			return false;
		}
		writeCount += bytes.length;
		return true;
	}

	@Override
	public boolean writeBuffer(MemorySegment buffer) {
		while (buffer.hasRemaining()) {
			byte toWrite = buffer.getByte();
			if (echo != null) {
				echo.writeChar(toWrite);
			}
			try {
				file.writeByte(toWrite);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@Override
	public int readCharUni() {
		try {
			readCount++;
			return file.readInt();
		} catch (EOFException e) {
			readCount--;
			return -1;
		} catch (IOException e) {
			throw new UnableToReadFileException("Failed reading byte", e);
		}
	}

	@Override
	public int readLineIntoBuffer(int addr, int len) {
		int[] codePoints = getLineFromFile(len).codePoints().toArray();
		for (int i = 0; i < codePoints.length; i++) {
			GLK.memory.put(addr + i, (byte) codePoints[i]);
		}
		// Null terminator is not part of read count
		readCount += codePoints.length - 1;
		return codePoints.length - 1;
	}

	@NotNull
	private String readLineUniFromFileIncludeNewlines() throws IOException {
		StringBuilder ret = new StringBuilder();
		int cp;
		//noinspection HardcodedLineSeparator
		do {
			try {
				cp = file.readInt();
			} catch (EOFException e) {
				break;
			}
			ret.appendCodePoint(cp);
		} while (cp != '\n');
		return ret.toString();
	}

	@NotNull
	private String getLineFromFile(int len) {
		String line;
		long startingPos;
		try {
			startingPos = file.getFilePointer();
		} catch (IOException e) {
			throw new UnableToReadFileException("Unable to get file position", e);
		}
		try {
			line = readLineUniFromFileIncludeNewlines();
		} catch (IOException e) {
			throw new UnableToReadFileException("Unable to read file line", e);
		}
		if (line.length() >= len) {
			line = line.substring(0, len - 1);
			try {
				file.seek(startingPos + len - 1);
			} catch (IOException e) {
				throw new UnableToReadFileException("Unable to seek file position", e);
			}
		}
		line += '\0';
		return line;
	}

	@Override
	public int readLineUniIntoBuffer(int addr, int len) {
		int[] codePoints = getLineFromFile(len).codePoints().toArray();
		GLK.memory.put(addr, codePoints);
		// Null terminator is not part of read count
		readCount += codePoints.length - 1;
		return codePoints.length - 1;
	}

	@Override
	public void writeCharUni(int c) {
		if (echo != null) {
			echo.writeCharUni(c);
		}
		try {
			file.writeInt(c);
			writeCount++;
		} catch (IOException e) {
			throw new UnableToWriteFileException("Unable to write int:" + c, e);
		}
	}

	@Override
	public void writeString(@NotNull String s) {
		if (echo != null) {
			echo.writeString(s);
		}
		s.codePoints().forEachOrdered(v -> {
			try {
				file.writeInt(v);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		writeCount += s.length();
	}
}
