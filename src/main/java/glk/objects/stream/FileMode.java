/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;

import static java.nio.file.StandardOpenOption.*;

public enum FileMode {
	WRITE(1, "rw", new OpenOption[]{
			TRUNCATE_EXISTING,
			StandardOpenOption.WRITE,
			CREATE
	}),
	READ(2, "r", new OpenOption[]{
			StandardOpenOption.READ
	}),
	READWRITE(3, "rw", new OpenOption[]{
			StandardOpenOption.READ,
			StandardOpenOption.WRITE,
			CREATE
	}),
	WRITE_APPEND(5, "rw", new OpenOption[]{
			APPEND,
			CREATE,
			StandardOpenOption.WRITE
	});

	private final int mode;
	private final OpenOption[] openOptions;
	private final String fileSystemMode;

	FileMode(int i, String fileSystemMode, OpenOption[] openOptions) {
		mode = i;
		this.openOptions = openOptions;
		this.fileSystemMode = fileSystemMode;
	}

	@NotNull
	public static FileMode get(int id) {
		for (FileMode f : FileMode.values()) {
			if (f.mode == id) {
				return f;
			}
		}
		throw new IllegalArgumentException("Unexpected value: " + id);
	}

	OpenOption[] getOpenOptions() {
		return this.openOptions;
	}

	@NotNull
	@Contract(pure = true)
	String getFilesystemMode() {
		return fileSystemMode;
	}
}
