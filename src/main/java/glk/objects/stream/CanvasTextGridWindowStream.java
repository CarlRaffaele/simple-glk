/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import glk.objects.Style;
import glk.objects.StyleHint;
import glk.objects.stream_result_t;
import javafx.animation.AnimationTimer;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import org.jetbrains.annotations.NotNull;

import java.util.Deque;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedDeque;

import static glk.objects.window.Window.*;

public class CanvasTextGridWindowStream extends BaseStyleableStream implements CursorStream, WritableStream {
	@NotNull private final GraphicsContext graphicsContext;
	@NotNull private final AnimationTimer timer;
	@NotNull private final Deque<PendingWrite> pendingWrites;
	private int x, y;

	private static record PendingWrite(int x, int y, int codePoint, Style style) {
	}

	CanvasTextGridWindowStream(int rock, @NotNull GraphicsContext graphicsContext, @NotNull Map<Style, Map<StyleHint, Integer>> styleHints) {
		super(FileMode.WRITE, rock, styleHints);
		this.graphicsContext = graphicsContext;
		graphicsContext.setTextBaseline(VPos.TOP);
		graphicsContext.setFont(Font.font("MONOSPACE", Style.DEFAULT_FONT_SIZE));
		x = 0;
		y = 0;
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				while (!pendingWrites.isEmpty()) {
					PendingWrite pw = pendingWrites.pop();
					if (pw.codePoint == -1) {
						clearStream();
					} else {
						switch (pw.style) {
							case Emphasized -> graphicsContext.setFont(Font.font("MONOSPACE", FontWeight.BOLD, Style.DEFAULT_FONT_SIZE));
							case Header -> graphicsContext.setFont(Font.font("MONOSPACE", FontWeight.BOLD,
									Style.DEFAULT_FONT_SIZE * 1.5));
							case Subheader -> graphicsContext.setFont(Font.font("MONOSPACE", FontWeight.BOLD,
									Style.DEFAULT_FONT_SIZE * 1.2));
							case Note -> graphicsContext.setFont(Font.font("MONOSPACE", FontPosture.ITALIC, Style.DEFAULT_FONT_SIZE));
							default -> graphicsContext.setFont(Font.font("MONOSPACE", Style.DEFAULT_FONT_SIZE));
						}
						graphicsContext.fillText(Character.toString(pw.codePoint), pw.x, pw.y);
					}
				}
			}
		};
		pendingWrites = new ConcurrentLinkedDeque<>();
	}

	@Override
	public stream_result_t close() {
		timer.stop();
		return super.close();
	}

	@Override
	public void setID(int id) {
		timer.start();
		super.setID(id);
	}

	@SuppressFBWarnings("UPM_UNCALLED_PRIVATE_METHOD")
	private void clearStream() {
		graphicsContext.setFill(Color.WHITE);
		graphicsContext.fillRect(0,
				0,
				graphicsContext.getCanvas().getWidth(),
				graphicsContext.getCanvas().getHeight());
		graphicsContext.setFill(Color.BLACK);
	}

	@Override
	public void clear() {
		pendingWrites.clear();
		pendingWrites.add(new PendingWrite(0, 0, -1, super.currentStyle));
	}

	@Override
	public void setCursorPosition(int x, int y) {
		this.x = x * M_WIDTH;
		this.y = y * M_HEIGHT;
	}

	@Override
	public void writeCharUni(int c) {
		if (echo != null) {
			echo.writeCharUni(c);
		}
		pendingWrites.add(new PendingWrite(x, y, c, super.currentStyle));
		x += M_WIDTH;
		if (c == NEWLINE) {
			x = 0;
			y += M_HEIGHT;
		}
		super.writeCount++;
	}

	@Override
	public void writeChar(byte b) {
		writeCharUni(b & 0xff);
	}

	@Override
	public void writeString(@NotNull String s) {
		s.codePoints().forEach(this::writeCharUni);
	}
}
