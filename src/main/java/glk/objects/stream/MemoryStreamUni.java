/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.GLK;
import org.jetbrains.annotations.NotNull;

class MemoryStreamUni extends BaseStream implements WritableStream, ReadableStream {
	private final int address, length;

	MemoryStreamUni(int bufAddress, int bufLen, FileMode fmode, int rock) {
		super(fmode, rock);
		super.size = bufAddress == 0 ? 0 : bufLen;
		address = bufAddress;
		length = bufLen;
	}

	protected int readCharUni(MemorySegment buffer) {
		if (failedRead()) {
			return -1;
		}
		position++;
		if (position > eof) {
			eof = position;
		}
		readCount++;
		return buffer.getInt(address + ((int) super.position * 4));
	}

	@Override
	public int readCharUni() {
		return this.readCharUni(GLK.memory);
	}

	@Override
	public int readIntoBufferUni(int addr, int len) {
		if (failedRead()) {
			return -1;
		}
		return StreamUtils.bulkReadIntoBufferUni(
				GLK.memory.slice(address, length).position((int) (super.position * 4)),
				this,
				addr,
				len);
	}

	protected void writeCharUni(int c, MemorySegment buffer) {
		failedWrite();
		if (echo != null) {
			echo.writeCharUni(c);
		}
		if (position < super.size) {
			buffer.put(address + ((int) super.position * 4), c);
			position++;
			if (position > eof) {
				eof = position;
			}
		}
		writeCount++;
	}

	@Override
	public void writeCharUni(int c) {
		writeCharUni(c, GLK.memory);
	}

	@Override
	public void writeString(@NotNull String s) {
		failedWrite();
		if (echo != null) {
			echo.writeString(s);
		}
		s.codePoints().forEachOrdered(this::writeCharUni);
	}

	@Override
	public void writeBufferUni(int addr, int len) {
		failedWrite();
		super.position += StreamUtils.bulkWriteIntoBufferUni(
				GLK.memory.slice(address, length).position((int) (super.position * 4)),
				this,
				addr,
				len);

	}
}
