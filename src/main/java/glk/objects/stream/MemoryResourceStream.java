/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;

public class MemoryResourceStream extends MemoryStream {
	private final MemorySegment buffer;

	MemoryResourceStream(@NotNull MemorySegment buffer, FileMode fmode, int rock) {
		super(-1, buffer.length(), fmode, rock);
		this.buffer = buffer;
	}

	@Override
	public void setPosition(long position, int seekmode) {
		super.setPosition(position, seekmode);
		buffer.position((int) super.position);
	}

	@Override
	public byte readChar() {
		return super.readChar(buffer);
	}

	@Override
	public void writeChar(byte b) {
		super.writeChar(b, buffer);
	}

	@Override
	public int readIntoBuffer(int addr, int len) {
		if (failedRead()) {
			return -1;
		}
		return StreamUtils.bulkReadIntoBuffer(buffer, this, addr, len);
	}

	@Override
	public void writeBuffer(int addr, int len) {
		failedWrite();
		StreamUtils.bulkWriteIntoBuffer(buffer, this, addr, len);
	}
}
