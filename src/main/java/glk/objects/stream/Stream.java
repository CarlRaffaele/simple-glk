/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

import blorbReader.BlorbChunkType;
import blorbReader.BlorbReader;
import blorbReader.RepresentableID;
import gameMemory.MemorySegment;
import glk.objects.GLKOpaqueObject;
import glk.objects.Style;
import glk.objects.StyleHint;
import glk.objects.stream_result_t;
import glk.objects.window.WindowTypes;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.web.WebEngine;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.Optional;

public interface Stream extends GLKOpaqueObject {
	stream_result_t close();

	boolean closed();

	boolean hasReadPermission();

	long getPosition();

	void setPosition(long position, int seekmode);

	boolean readOnly();

	void notifyMemoryResize();

	void setEcho(WritableStream echo);

	Optional<WritableStream> getEcho();

	void setHyperlink(int linkValue);

	void clear();

	@NotNull
	static Stream windowStreamFrom(@NotNull WindowTypes type,
								   FileMode fileMode,
								   int rock,
								   WebEngine webEngine,
								   GraphicsContext graphicsContext,
								   Map<Style, Map<StyleHint, Integer>> styleHints) {
		return switch (type) {
			case TEXT_BUFFER -> new WebTextFlowWindowStream(fileMode, rock, webEngine, styleHints);
			case BLANK, GRAPHICS -> new NOPStream(rock);
			case TEXT_GRID -> new CanvasTextGridWindowStream(rock, graphicsContext, styleHints);
			case ALL, PAIR -> throw new IllegalArgumentException("Tried to create a stream for an Illegal window type: " + type);
		};
	}

	@NotNull
	static Stream memoryStreamFrom(boolean unicode, int address, int length, int fmode, int rock) {
		return unicode ?
				new MemoryStreamUni(address, length, FileMode.get(fmode), rock) :
				new MemoryStream(address, length, FileMode.get(fmode), rock);
	}

	@NotNull
	static Stream fileStreamFrom(@NotNull FileReference fileRef, FileMode fileMode, boolean isUnicode, int rock) {
		if (fileRef.isTextMode()) {
			if (isUnicode) {
				return new ReadableWritableTextFileStreamUni(fileRef, fileMode, rock);
			} else {
				return new ReadableWritableTextFileStream(fileRef, fileMode, rock);
			}
		} else {
			if (isUnicode) {
				return new ReadableWritableBinaryFileStreamUni(fileRef, fileMode, rock);
			} else {
				return new ReadableWritableBinaryFileStream(fileRef, fileMode, rock);
			}
		}
	}

	@Nullable
	static ReadableStream resourceStreamFrom(int ID, int rock, boolean unicode, @NotNull BlorbReader blorbReader) {
		if (blorbReader.getDataType(ID).isEmpty()) {
			System.err.println("Unable to find resource: " + ID);
			return null;
		}
		RepresentableID type = blorbReader.getDataType(ID).get();
		if (BlorbChunkType.TEXT == type) { // Text stream
			if (unicode) {
				return new TextResourceStreamUni(rock, blorbReader.getData(ID).orElseThrow());
			} else {
				return new TextResourceStream(rock, blorbReader.getData(ID).orElseThrow());
			}
		} else { // Binary stream
			MemorySegment buffer;
			if (BlorbChunkType.FORM == type) {
				buffer = blorbReader.getDataWithHeader(ID).orElseThrow();
			} else {
				buffer = blorbReader.getData(ID).orElseThrow();
			}
			if (unicode) {
				return new MemoryResourceStreamUni(buffer, FileMode.READ, rock);
			} else {
				return new MemoryResourceStream(buffer, FileMode.READ, rock);
			}
		}
	}
}

