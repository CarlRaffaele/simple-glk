/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.GLK;

/**
 * WritableStream is a version of a {@link Stream} that can be written to. Examples would be a {@link MemoryStream}
 * or a {@link FileStream}.
 */
public interface WritableStream extends Stream {
	/**
	 * Write a Unicode code point to the stream. From the spec:
	 * <p>
	 * You can print the newline character (linefeed, control-J, decimal 10,
	 * hex 0x0A.) It is *not* legal to print any other control characters (0 to 9, 11 to 31,
	 * 127 to 159). You may not print even common formatting characters such as
	 * tab (control-I), carriage return (control-M), or page break (control-L).
	 *
	 * @param c The codepoint to write
	 */
	default void writeCharUni(int c) {
		//default implementation for non-unicode streams
		this.writeChar((byte) (c > 255 ? '?' : c));
	}

	/**
	 * Write a Latin-1 character to the stream. From the spec:
	 * <p>
	 * When you are sending text to a window, or to a file open in text mode, you
	 * can print any of the printable Latin-1 characters: 32 to 126, 160 to 255.
	 * You can also print the newline character (linefeed, control-J, decimal 10,
	 * hex 0x0A.)
	 * <p>
	 * It is *not* legal to print any other control characters (0 to 9, 11 to 31,
	 * 127 to 159). You may not print even common formatting characters such as
	 * tab (control-I), carriage return (control-M), or page break (control-L).
	 *
	 * @param b The character to write
	 */
	default void writeChar(byte b) {
		//default implementation for unicode streams
		this.writeCharUni(b & 0xFF);
	}

	/**
	 * Write the string version of an integer to the stream. This is the equivalent of Integer(c).toString().
	 *
	 * @param c The integer to write as a string
	 */
	default void writeInt(int c) {
		writeString(Integer.toString(c));
	}

	/**
	 * Write a Java String object to the stream. How this is done is dependent on the implementation. A Unicode
	 * stream should implement this in a Unicode tolerant way; however, a non-Unicode stream will not.
	 *
	 * @param s The String object to write
	 */
	void writeString(String s);

	/**
	 * Given a memory address, parse a string and write to it using the writeChar function. If a Unicode String is
	 * found then all the codepoints are downcast to byte and then written.
	 *
	 * @param addr The address of the String in main memory
	 * @throws IllegalArgumentException If an E0 or E2 byte is not found at the given address
	 */
	default void writeStringFromAddress(int addr) {
		MemorySegment gameData = GLK.memory.duplicate().position(addr);
		byte stringType = gameData.getByte();
		switch (stringType) {
			case (byte) 0xE0: // C-Style String
				while (true) {
					byte next = gameData.getByte();
					if (next == 0) {
						return;
					}
					writeChar(next);
				}
			case (byte) 0xE2: // Unicode String
				//Three bytes of padding
				gameData.getByte();
				gameData.getByte();
				gameData.getByte();
				while (true) {
					int toAdd = gameData.getInt();
					if (toAdd == 0x00000000) {
						return;
					}
					writeChar((byte) toAdd);
				}
			default:
				throw new IllegalArgumentException("Illegal string given to GLK to parse");
		}
	}

	/**
	 * Given a memory address, parse a string and write to it using the writeCharUni function.
	 *
	 * @param addr The address of the String in main memory
	 * @throws IllegalArgumentException If an E0 or E2 byte is not found at the given address
	 */
	default void writeStringFromAddressUni(int addr) {
		MemorySegment gameData = GLK.memory.duplicate().position(addr);
		byte stringType = gameData.getByte();
		switch (stringType) {
			case (byte) 0xE0: // C-Style String
				while (true) {
					byte next = gameData.getByte();
					if (next == 0) {
						return;
					}
					writeCharUni(next & 0xff);
				}
			case (byte) 0xE2: // Unicode String
				//Three bytes of padding
				gameData.getByte();
				gameData.getByte();
				gameData.getByte();
				while (true) {
					int toAdd = gameData.getInt();
					if (toAdd == 0x00000000) {
						return;
					}
					writeCharUni(toAdd);
				}
			default:
				throw new IllegalArgumentException("Illegal string given to GLK to parse");
		}
	}

	/**
	 * Write a block of Latin-1 characters to the stream.
	 *
	 * @param addr The address of the buffer to read from
	 * @param len  The length of the buffer
	 */
	default void writeBuffer(int addr, int len) {
		if (closed()) {
			throw new NullPointerException("Tried to write to a closed stream");
		}
		if (readOnly()) {
			throw new IllegalArgumentException("Tried to write to a stream without write permission");
		}
		for (int i = 0; i < len; i++) {
			writeChar(GLK.memory.getByte(addr + i));
		}
	}

	/**
	 * Write a block of Unicode characters to the stream.
	 *
	 * @param addr The address of the buffer to read from
	 * @param len  The length of the buffer
	 */
	default void writeBufferUni(int addr, int len) {
		if (closed()) {
			throw new NullPointerException("Tried to write to a closed stream");
		}
		if (readOnly()) {
			throw new IllegalArgumentException("Tried to write to a stream without write permission");
		}
		for (int i = 0; i < len * 4; i += 4) {
			writeCharUni(GLK.memory.getInt(addr + i));
		}
	}

	default void writeImage(String image, int val1, int val2) {
		getEcho().ifPresent(s -> s.writeImage(image, val1, val2));
	}

	default void writeImageScaled(String image, int val1, int val2, int width, int height) {
		getEcho().ifPresent(s -> s.writeImageScaled(image, val1, val2, width, height));
	}

	default void insertFlowBreak() {
		//most streams will do nothing.
	}
}
