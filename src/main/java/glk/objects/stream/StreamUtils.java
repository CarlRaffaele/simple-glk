/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.GLK;
import org.jetbrains.annotations.NotNull;

final class StreamUtils {
	static int bulkReadIntoBuffer(@NotNull MemorySegment data, @NotNull BaseStream stream, int addr, int len) {
		int amountToRead = Math.min(len, data.remaining());
		GLK.memory.slice(addr, amountToRead).put(data.slice(data.position(), amountToRead));
		data.position(data.position() + amountToRead);
		stream.position += amountToRead;
		if (stream.position > stream.eof) {
			stream.eof = stream.position;
		}
		stream.readCount += amountToRead;
		return amountToRead;
	}

	static int bulkReadIntoBufferUni(@NotNull MemorySegment data, @NotNull BaseStream stream, int addr, int len) {
		int amountToRead = Math.min(len, data.remaining());
		GLK.memory.slice(addr, amountToRead * 4).put(data.slice(data.position(), amountToRead * 4));
		data.position(data.position() + amountToRead);
		stream.position += amountToRead;
		if (stream.position > stream.eof) {
			stream.eof = stream.position;
		}
		stream.readCount += amountToRead;
		return amountToRead;
	}

	static int bulkWriteIntoBuffer(@NotNull MemorySegment data, @NotNull BaseStream stream, int addr, int len) {
		int amountToWrite = Math.min(len, data.remaining());
		data.slice(data.position(), amountToWrite).put(GLK.memory.slice(addr, amountToWrite));
		data.position(data.position() + amountToWrite);
		if (stream.position > stream.eof) {
			stream.eof = stream.position;
		}
		stream.writeCount += amountToWrite;
		return amountToWrite;
	}

	static int bulkWriteIntoBufferUni(@NotNull MemorySegment data, @NotNull BaseStream stream, int addr, int len) {
		int amountToWrite = Math.min(len, data.remaining());
		data.slice(data.position(), amountToWrite * 4).put(GLK.memory.slice(addr, amountToWrite * 4));
		data.position(data.position() + amountToWrite);
		if (stream.position > stream.eof) {
			stream.eof = stream.position;
		}
		stream.writeCount += amountToWrite;
		return amountToWrite;
	}
}
