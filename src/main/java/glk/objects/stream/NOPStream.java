/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

public class NOPStream extends BaseStream implements WritableStream {
	NOPStream(int rock) {
		super(FileMode.WRITE, rock);
	}

	@Override
	public void clear() {

	}

	@Override
	public void writeCharUni(int c) {

	}

	@Override
	public void writeChar(byte b) {

	}

	@Override
	public void writeString(String s) {

	}
}
