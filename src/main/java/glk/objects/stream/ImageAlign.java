/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

public enum ImageAlign {
	INLINE_UP {
		@Override
		public String asHtml() {
			return "display: inline-block; vertical-align: baseline";
		}
	},
	INLINE_DOWN {
		@Override
		public String asHtml() {
			return "display: inline-block; vertical-align: text-top";
		}
	},
	INLINE_CENTER {
		@Override
		public String asHtml() {
			return "display: inline-block; vertical-align: middle";
		}
	},
	MARGIN_LEFT {
		@Override
		public String asHtml() {
			return "float: left";
		}
	},
	MARGIN_RIGHT {
		@Override
		public String asHtml() {
			return "float: right";
		}
	};

	public static ImageAlign byID(int id) {
		return switch (id) {
			case 1 -> INLINE_UP;
			case 2 -> INLINE_DOWN;
			case 3 -> INLINE_CENTER;
			case 4 -> MARGIN_LEFT;
			case 5 -> MARGIN_RIGHT;
			default -> throw new IllegalArgumentException("Unknown image alignment: " + id);
		};
	}

	public abstract String asHtml();
}
