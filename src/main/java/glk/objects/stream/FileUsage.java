/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public enum FileUsage {
	DATA(".glkdata"),
	SAVED_GAME(".glsave"),
	TRANSCRIPT(".txt"),
	INPUT_RECORD(".txt");

	private final String fileExtension;

	FileUsage(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public static FileUsage get(int id) {
		return FileUsage.values()[id];
	}

	@NotNull
	@Contract(pure = true)
	public String getExtension() {
		return fileExtension;
	}

	@NotNull
	@Contract(pure = true)
	public static String getExtension(int fileMode) {
		return FileUsage.get(fileMode & 0x7).getExtension();
	}
}
