/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;

public class TextResourceStream extends BaseStream implements ReadableStream {
	private final MemorySegment data;

	TextResourceStream(int rock, @NotNull MemorySegment data) {
		super(FileMode.READ, rock);
		this.data = data;
		super.eof = data.length();
		super.size = data.length();
	}

	@Override
	public void setPosition(long position, int seekmode) {
		super.setPosition(position, seekmode);
		data.position((int) super.position);
	}

	@Override
	public int readIntoBuffer(int addr, int len) {
		if (failedRead()) {
			return -1;
		}
		return StreamUtils.bulkReadIntoBuffer(data, this, addr, len);
	}


	@Override
	public byte readChar() {
		if (!data.hasRemaining()) {
			return -1;
		}
		readCount++;
		position++;
		return data.getByte();
	}
}
