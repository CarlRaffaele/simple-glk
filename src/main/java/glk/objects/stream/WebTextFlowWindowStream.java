/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import glk.objects.Style;
import glk.objects.StyleHint;
import glk.objects.stream_result_t;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.web.WebEngine;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.html.HTMLAnchorElement;
import org.w3c.dom.html.HTMLImageElement;

import java.util.Deque;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;

class WebTextFlowWindowStream extends BaseStyleableStream implements WritableStream {
	@NotNull private final StringBuffer buffer;
	@NotNull private final WebEngine webEngine;
	private final AnimationTimer timer;
	private final Deque<PendingWrite> pendingWrites;
	private Element element;

	public WebTextFlowWindowStream(FileMode fmode, int rock, @NotNull WebEngine webEngine,
								   Map<Style, Map<StyleHint, Integer>> styleHints) {
		super(fmode, rock, styleHints);
		this.webEngine = webEngine;
		buffer = new StringBuffer();
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				updateOutput();
			}
		};
		pendingWrites = new ConcurrentLinkedDeque<>();

	}

	private record PendingWrite(String value,
								Style style,
								String styleHint,
								boolean newLineFollows,
								boolean isImage,
								int hyperlinkValue) {

		private PendingWrite(String value, boolean newLineFollows, Style style, String styleHint, int hyperlinkValue) {
			this(value, style, styleHint, newLineFollows, false, hyperlinkValue);
		}

		private PendingWrite(String b64img, Style style, String styleHint, int hyperlinkValue) {
			this(b64img, style, styleHint, false, true, hyperlinkValue);
		}
	}

	@Override
	public stream_result_t close() {
		timer.stop();
		return super.close();
	}

	private void setupElements() {
		Document doc = webEngine.getDocument();
		this.element = doc.getElementById("mainArea");
		element.setAttribute("class", "textBuffer");
	}

	@Override
	public void setID(int id) {
		super.setID(id);
		Platform.runLater(() -> {
			setupElements();
			timer.start();
		});
	}

	@Override
	public void setStyle(@Nullable Style style) {
		flushBuffer(false);
		super.setStyle(style);
	}

	@Override
	public void writeCharUni(int c) {
		if (echo != null) {
			echo.writeCharUni(c);
		}
		super.writeCount += 1;

		if (c == '\n' || c == '\r') {
			flushBuffer(true);
			return;
		}

		if (Character.isValidCodePoint(c)) {
			buffer.appendCodePoint(c);
		} else {
			buffer.append('?');
		}
	}

	private void flushBuffer(boolean insertBreakAfterBuffer) {
		pendingWrites.add(new PendingWrite(buffer.toString(), insertBreakAfterBuffer, currentStyle, styleHints.get(currentStyle),
				super.linkValue));
		buffer.setLength(0);
	}

	private void updateOutput() {
		if (super.linkValue != 0) { //queue up link to write all at once
			return;
		}
		if (!buffer.isEmpty()) {
			flushBuffer(false);
		}

		//WARNING: be careful with what elements are allowed to be shown to the user. Allowing tags that access the
		// internet or really anything outside the game pose a security risk.
		if (!pendingWrites.isEmpty()) {
			if (Platform.isFxApplicationThread()) {
				updateOutputFX();
			} else {
				Platform.runLater(this::updateOutputFX);
			}
		}
	}

	private void updateOutputFX() {
		Boolean scrolledDown = (Boolean) webEngine.executeScript("(window.scrollY + window.innerHeight) >= " +
				"(document.body.offsetHeight - 2);");
		while (!pendingWrites.isEmpty()) {
			PendingWrite pw = pendingWrites.pop();
			Element add;
			if (pw.isImage) {
				add = webEngine.getDocument().createElement("img");
				((HTMLImageElement) add).setSrc(pw.value);
			} else {
				if (pw.hyperlinkValue == 0) {
					add = webEngine.getDocument().createElement("text");
				} else {
					add = webEngine.getDocument().createElement("a");
					((HTMLAnchorElement) add).setHref("javascript:onHyperlink(" + pw.hyperlinkValue + ");");
				}
				add.setTextContent(pw.value);
			}
			add.setAttribute("class", "style" + pw.style());
			add.setAttribute("style", pw.styleHint());
			element.appendChild(add);
			if (pw.newLineFollows) {
				element.appendChild(webEngine.getDocument().createElement("br"));
			}
		}
		if (scrolledDown) {
			webEngine.executeScript("document.getElementById('mainArea').scrollIntoView(false);");
		}
	}

	@Override
	public void setHyperlink(int linkValue) {
		flushBuffer(false);
		super.setHyperlink(linkValue);
	}

	@Override
	public void writeChar(byte b) {
		writeCharUni(b & 0xff);
	}

	@Override
	public void writeString(String s) {
		if (echo != null) {
			echo.writeString(s);
		}
		buffer.append(s);
		super.writeCount += s.length();
	}

	public void clear() {
		buffer.setLength(0);
		pendingWrites.clear();
		Platform.runLater(() -> {
			Node body = webEngine.getDocument().getElementsByTagName("body").item(0);
			body.removeChild(element);
			Element newMain = webEngine.getDocument().createElement("div");
			newMain.setAttribute("id", "mainArea");
			body.appendChild(newMain);
			setupElements();
		});
	}

	@Override
	public void writeImage(String imgB64, int val1, int val2) {
		String style = ImageAlign.byID(val1).asHtml();
		pendingWrites.add(new PendingWrite(imgB64, currentStyle, style, super.linkValue));
	}

	@Override
	public void writeImageScaled(String imgB64, int val1, int val2, int width, int height) {
		String style = "width: %dpx; height: %dpx; %s".formatted(width, height, ImageAlign.byID(val1).asHtml());
		pendingWrites.add(new PendingWrite(imgB64, currentStyle, style, super.linkValue));
	}

	@Override
	public void insertFlowBreak() {
		AtomicBoolean done = new AtomicBoolean(false);
		Platform.runLater(() -> {
			updateOutput(); // clear what's there
			//append a new div that clears any float elements
			Element toAdd = webEngine.getDocument().createElement("div");
			toAdd.setAttribute("style", "clear:both;");
			element.appendChild(toAdd);
			done.set(true);
		});
		while (!done.get()) {
			Thread.onSpinWait();
		}
	}
}
