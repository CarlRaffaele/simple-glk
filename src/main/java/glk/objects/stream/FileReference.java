/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import glk.GLK;
import glk.objects.GLKOpaqueObject;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public interface FileReference extends GLKOpaqueObject {
	String TEMP_PREFIX = "glk_";
	String TEMP_EXTENSION = ".glktmp";

	@NotNull
	private static String replaceInvalidFilename(@NotNull String fileName) {
		String ret = fileName
				.replaceAll("[/\\\\<>:\"|?*]", "") // remove invalid characters
				.split("\\.")[0]; // everything after the first '.' is ignored
		if (ret.isBlank()) {
			ret = "null";
		}
		return ret;
	}

	@NotNull
	@Contract("_, _, _ -> new")
	static FileRef CreateByName(int usage, int nameAddr, int rock) {
		String name = replaceInvalidFilename(GLK.stringFromAddress(nameAddr));
		return new FileRef(usage, rock, Path.of(name + FileUsage.getExtension(usage)), false);
	}

	@NotNull
	@Contract("_, _, _ -> new")
	static FileRef CreateByFileref(int usage, @NotNull FileReference fileRef, int rock) {
		return new FileRef(usage, rock, fileRef.getPath(), false);
	}

	@NotNull
	@Contract("_, _ -> new")
	static FileRef CreateTemp(int usage, int rock) {
		try {
			return new FileRef(usage, rock, Files.createTempFile(TEMP_PREFIX, TEMP_EXTENSION), true);
		} catch (IOException e) {
			throw new FileReferenceException("Failed to create temporary file.", e);
		}
	}

	@Nullable
	static FileRef CreateByPrompt(int usage, int rock, Window mainWindow) {
		FileChooser fc = new FileChooser();
		fc.setTitle("Select a File");
		File file = fc.showOpenDialog(mainWindow);
		if (file == null) {
			return null;
		}
		return new FileRef(usage, rock, file.toPath(), false);
	}

	void delete_file();

	boolean does_file_exist();

	boolean isTextMode();

	void destroy();

	void addUsage(FileStream fileStream);

	void removeUsage(FileStream fileStream);

	Path getPath();

	class FileReferenceException extends RuntimeException {
		FileReferenceException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}

