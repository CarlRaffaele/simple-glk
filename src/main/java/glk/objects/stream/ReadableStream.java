/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.GLK;

public interface ReadableStream extends Stream {

	/**
	 * Reads one character of from the stream. The result will be between 0 and 255. As with all basic text functions,
	 * Glk assumes the Latin-1 encoding. If the end of the stream has been reached, the result will be -1. If the stream
	 * contains Unicode data -- for example, if it was created with glk_stream_open_file_uni() or
	 * glk_stream_open_memory_uni() -- then characters beyond 255 will be returned as 0x3F ("?").
	 *
	 * @return The character read
	 */
	default byte readChar() {
		//default implementation for unicode streams
		int readInt = readCharUni();
		if (readInt > 255) {
			readInt = 0x3f; //Read a unicode char in a non-unicode way
		}
		return (byte) readInt;
	}

	/**
	 * Reads one character from the given stream. If the end of the stream has
	 * been reached, the result will be -1.
	 *
	 * @return The codepoint read
	 */
	default int readCharUni() {
		//default implementation for non-unicode streams
		int rc = readChar();
		return rc == -1 ? -1 : rc & 0xff;
	}

	/**
	 * This reads len characters from the given stream, unless the end of stream
	 * is reached first. No terminal null is placed in the buffer. It returns the
	 * number of characters actually read.
	 *
	 * @param addr The address of the buffer to put the result
	 * @param len  The max number of characters to read
	 * @return The number of characters read
	 */
	default int readIntoBuffer(int addr, int len) {
		int ret = 0;
		for (int i = 0; i < len; i++, ret++) {
			byte read = readChar();
			if (read == -1) {
				break;
			}
			GLK.memory.put(addr + i, read);
		}
		return ret;
	}

	/**
	 * This reads len Unicode characters from the given stream, unless the end of
	 * the stream is reached first. No terminal null is placed in the buffer.  It
	 * returns the number of Unicode characters actually read.
	 *
	 * @param addr The address of the buffer to put the result
	 * @param len  The max number of codepoints to read
	 * @return The number of codepoints read
	 */
	default int readIntoBufferUni(int addr, int len) {
		int ret = 0;
		for (int i = 0; i < len; i++, ret++) {
			int read = readCharUni();
			if (read == -1) {
				break;
			}
			GLK.memory.put(addr + (i * 4), read);
		}
		return ret;
	}

	/**
	 * This reads characters from the given stream, until either len-1 characters
	 * have been read or a newline has been read. It then puts a terminal null
	 * ('\0') character on the end. It returns the number of characters actually
	 * read, including the newline (if there is one) but not including the
	 * terminal null.
	 *
	 * @param addr The address of the buffer to put the result
	 * @param len  The max number of characters to read
	 * @return The number of characters read
	 */
	default int readLineIntoBuffer(int addr, int len) {
		//Assumes the two buffer don't overlap
		MemorySegment buffer = GLK.memory.slice(addr, len);
		int amountRead = 0;
		for (int i = 0, readChar = readChar();
			 i < (len - 1) && readChar != -1;
			 i++, amountRead++, readChar = readChar()) {
			buffer.put((byte) readChar);
			//noinspection HardcodedLineSeparator
			if (readChar == '\n') {
				amountRead++;
				break;
			}
		}
		buffer.put((byte) '\0'); //null-terminate
		return amountRead;
	}

	/**
	 * This reads Unicode characters from the given stream, until either len-1
	 * Unicode characters have been read or a newline has been read. It then puts
	 * a terminal null (a zero value) on the end. It returns the number of Unicode
	 * characters actually read, including the newline (if there is one) but not
	 * including the terminal null.
	 *
	 * @param addr The address of the buffer to put the result
	 * @param len  The max number of codepoints to read
	 * @return The number of codepoints read
	 */
	default int readLineUniIntoBuffer(int addr, int len) {
		MemorySegment buffer = GLK.memory.slice(addr, len * 4);
		int amountRead = 0;
		for (int i = 0, codepoint = readCharUni();
			 i < len - 1 && codepoint != -1;
			 i++, amountRead++, codepoint = readCharUni()) {
			buffer.put(codepoint);
			//noinspection HardcodedLineSeparator
			if (codepoint == '\n') {
				amountRead++;
				break;
			}
		}
		buffer.put(0);
		return amountRead;
	}
}
