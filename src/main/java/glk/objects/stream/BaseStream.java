/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import glk.GLK;
import glk.objects.stream_result_t;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

abstract class BaseStream implements Stream {
	private final int rock;
	private final FileMode fmode;
	long position, eof;
	int readCount;
	int writeCount;
	protected long size;
	private int uniqueValue;
	private boolean isOpen;
	@Nullable protected WritableStream echo;
	protected int linkValue;

	BaseStream(FileMode fmode, int rock) {
		isOpen = true;
		this.rock = rock;
		this.fmode = fmode;
	}

	boolean failedRead() {
		if (closed()) {
			throw new NullPointerException("Tried to read from a closed stream");
		}
		if (!hasReadPermission()) {
			throw new IllegalArgumentException("Tried to read from a stream without read permission");
		}
		return position >= size;
	}

	void failedWrite() {
		if (closed()) {
			throw new NullPointerException("Tried to write to a closed stream");
		}
		if (readOnly()) {
			throw new IllegalArgumentException("Tried to write to a stream without write permission");
		}
	}

	@Override
	public stream_result_t close() {
		if (!isOpen) {
			throw new NullPointerException("Tried to close a closed stream");
		}
		isOpen = false;
		return new stream_result_t(readCount, writeCount);
	}

	@Override
	public boolean closed() {
		return !isOpen;
	}

	@Override
	public boolean hasReadPermission() {
		return fmode == FileMode.READ || fmode == FileMode.READWRITE;
	}

	@Override
	public boolean readOnly() {
		return fmode != FileMode.WRITE && fmode != FileMode.READWRITE && fmode != FileMode.WRITE_APPEND;
	}

	@Override
	public long getPosition() {
		return position;
	}

	long getEof() {
		return eof;
	}

	@Override
	public void setPosition(long position, int seekmode) {
		switch (Seekmode.get(seekmode)) {
			case START -> this.position = position;
			case CURRENT -> this.position += position;
			case END -> this.position = getEof() + position;
			default -> System.err.println("Unknown seekmode: " + seekmode);
		}
	}

	@Override
	public int getRock() {
		return rock;
	}

	@Override
	public void setID(int id) {
		this.uniqueValue = id;
	}

	@Override
	public final int hashCode() {
		return uniqueValue;
	}

	@Contract(value = "null -> false", pure = true)
	@Override
	public final boolean equals(Object o) {
		return o instanceof Stream && this.hashCode() == o.hashCode();
	}

	@Override
	public String toString() {
		return "Stream [" + ", isOpen=" + isOpen + ", uniqueValue=" + uniqueValue + ", rock=" + rock
				+ ", fmode=" + fmode + ", position=" + position + ", readCount=" + readCount + ", writeCount=" + writeCount
				+ "]";
	}

	@Override
	public void writeUniqueValue(int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push(uniqueValue);
		} else {
			GLK.memory.put(address, uniqueValue);
		}
	}

	@Override
	public void notifyMemoryResize() {
		//Most streams don't need to do anything, but if they do then they should override this method.
	}

	@Override
	public void setEcho(@Nullable WritableStream echo) {
		this.echo = echo;
	}

	@Override
	public Optional<WritableStream> getEcho() {
		return Optional.ofNullable(echo);
	}

	@Override
	public void setHyperlink(int linkValue) {
		this.linkValue = linkValue;
	}

	@Override
	public void clear() {
	}
}
