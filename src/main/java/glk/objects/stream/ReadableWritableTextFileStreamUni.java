/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.objects.stream_result_t;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.*;
import java.nio.file.Files;

public class ReadableWritableTextFileStreamUni extends BaseFileStream implements ReadableStream, WritableStream {

	private final SeekableByteChannel file;
	private final ByteBuffer temp;
	private final CharsetEncoder encoder;
	private final CharsetDecoder decoder;

	ReadableWritableTextFileStreamUni(FileReference fileReference, FileMode fmode, int rock) {
		super(fileReference, fmode, rock);
		try {
			file = Files.newByteChannel(fileReference.getPath(), fmode.getOpenOptions());
		} catch (IOException e) {
			throw new UnableToReadFileException("Unable to open file: " + fileReference, e);
		}
		temp = ByteBuffer.wrap(new byte[4]).limit(1);
		encoder = StandardCharsets.UTF_8.newEncoder();
		encoder.onMalformedInput(CodingErrorAction.REPORT);
		encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
		decoder = StandardCharsets.UTF_8.newDecoder();
		decoder.onMalformedInput(CodingErrorAction.REPORT);
		decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
	}

	@Override
	public stream_result_t close() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error closing file stream.");
		}
		return super.close();
	}

	@Override
	public long getPosition() {
		try {
			return file.position();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error getting filestream position.");
			return -1;
		}
	}

	@Override
	public void setPosition(long position, int seekmode) {
		try {
			file.position(position);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error setting position of file.");
			return;
		}
		super.position = getPosition();
		super.setPosition(position, seekmode);
	}

	@Override
	long getEof() {
		try {
			return file.size();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error getting size of file.");
			return -1;
		}
	}

	@Override
	public boolean writeBuffer(byte[] bytes) {
		if (echo != null) {
			for (byte b : bytes) {
				echo.writeChar(b);
			}
		}
		try {
			file.write(ByteBuffer.wrap(bytes));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean writeBuffer(MemorySegment buffer) {
		throw new UnsupportedOperationException();
		//		if (echo != null) {
//			buffer.mark();
//			while (buffer.hasRemaining()) {
//				echo.writeChar(buffer.getByte());
//			}
//			buffer.reset();
//		}
//		try {
//			file.write(buffer);
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
//		return true;
	}

	@Override
	public byte readChar() {
		try {
			if (file.read(temp) == -1) {
				return -1;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
		readCount++;
		return temp.get(0);
	}

	@Override
	public int readCharUni() {
		temp.rewind();
		try {
			if (file.read(temp) == -1) {
				return -1;
			}
			//UTF-8 Continuation bytes
			if ((temp.get(0) & 0b10000000) == 0) {
				readCount++;
				return temp.get(0) & 0xff;
			} else if ((temp.get(0) & 32) == 0) {
				temp.limit(2);
			} else if ((temp.get(0) & 16) == 0) {
				temp.limit(3);
			} else if ((temp.get(0) & 8) == 0) {
				temp.limit(4);
			}
			file.read(temp);
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}

		int ret;
		try {
			ret = decoder.decode(temp.rewind()).codePoints().findFirst().orElseThrow();
		} catch (CharacterCodingException e) {
			throw new UnableToReadFileException("Unable to decode read: ", e);
		}

		readCount++;
		temp.limit(1);
		return ret;
	}

	@Override
	public int readLineIntoBuffer(int addr, int len) {
		throw new UnsupportedOperationException();
//		file.mark();
//		while (file.hasRemaining() && file.get() != '\n') {
//		}
//		int end = file.position();
//		file.reset();
//		int lineLength = end - file.position() - 1;
//		return readIntoBuffer(addr, Math.min(len, lineLength));
	}

	@Override
	public int readLineUniIntoBuffer(int addr, int len) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void writeCharUni(int c) {
		if (echo != null) {
			echo.writeCharUni(c);
		}
		try {
			file.write(encoder.encode(CharBuffer.wrap(Character.toString(c))));
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Unable to writechar to file.");
			return;
		}
		writeCount++;
	}

	@Override
	public void writeString(String s) {
		if (echo != null) {
			echo.writeString(s);
		}
		byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
		try {
			file.write(ByteBuffer.wrap(bytes));
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Unable to writestring to file.");
			return;
		}
		writeCount += bytes.length;
	}
}
