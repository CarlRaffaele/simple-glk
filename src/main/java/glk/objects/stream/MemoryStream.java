/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.stream;

import gameMemory.MemorySegment;
import glk.GLK;
import org.jetbrains.annotations.NotNull;

class MemoryStream extends BaseStream implements WritableStream, ReadableStream {
	private final int address, length;

	MemoryStream(int bufferAddress, int bufferLength, FileMode fmode, int rock) {
		super(fmode, rock);
		super.size = bufferAddress == 0 ? 0 : bufferLength;
		address = bufferAddress;
		length = bufferLength;
		this.notifyMemoryResize();
	}

	protected byte readChar(MemorySegment buffer) {
		if (failedRead()) {
			return -1;
		}
		position++;
		if (position > eof) {
			eof = position;
		}
		readCount++;
		return buffer.getByte((int) (address + position));
	}

	@Override
	public byte readChar() {
		return readChar(GLK.memory);
	}

	@Override
	public void writeString(@NotNull String s) {
		failedWrite();
		if (echo != null) {
			echo.writeString(s);
		}
		s.chars().forEachOrdered(this::writeCharUni);
	}

	protected void writeChar(byte b, MemorySegment buffer) {
		failedWrite();
		if (echo != null) {
			echo.writeChar(b);
		}
		if (position < super.size) {
			buffer.put((int) (address + position), b);
			position++;
			if (position > eof) {
				eof = position;
			}
		} // Any output past the end of the buffer is ignored

		writeCount++;
	}

	@Override
	public void writeChar(byte b) {
		this.writeChar(b, GLK.memory);
	}

	@Override
	public int readIntoBuffer(int addr, int len) {
		if (failedRead()) {
			return -1;
		}
		return StreamUtils.bulkReadIntoBuffer(GLK.memory.slice(address, length)
														.position((int) super.position), this, addr, len);
	}

	@Override
	public void writeBuffer(int addr, int len) {
		failedWrite();
		super.position += StreamUtils.bulkWriteIntoBuffer(
				GLK.memory.slice(address, length).position((int) super.position),
				this,
				addr,
				len);
	}
}
