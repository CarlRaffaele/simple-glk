/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.stream;

import glk.objects.Style;
import glk.objects.StyleHint;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

public class BaseStyleableStream extends BaseStream implements StyleableStream {
	@NotNull @Unmodifiable protected final Map<Style, String> styleHints;
	@Nullable protected Style currentStyle;

	BaseStyleableStream(FileMode fileMode, int rock, @NotNull Map<Style, Map<StyleHint, Integer>> styleHints){
		super(fileMode, rock);
		currentStyle = Style.Normal;
		echo = null;
		Map<Style, String> tempMap = new EnumMap<>(Style.class);
		for (Style s : Style.values()) {
			tempMap.put(s, "");
		}
		for (Map.Entry<Style, Map<StyleHint, Integer>> style : styleHints.entrySet()) {
			StringBuilder newStyle = new StringBuilder();
			for (StyleHint sh : style.getValue().keySet()) {
				int value = style.getValue().get(sh);
				newStyle.append(sh.asHTML(value));
			}
			tempMap.put(style.getKey(), newStyle.toString());
		}
		this.styleHints = Collections.unmodifiableMap(tempMap);
	}

	@Override
	public void setStyle(@Nullable Style style) {
		currentStyle = style;
		if (echo != null && echo instanceof StyleableStream) {
			((StyleableStream) echo).setStyle(style);
		}
	}

	@Override
	public @NotNull @Unmodifiable Map<Style, String> getStyleHints() {
		return styleHints;
	}

	@Override
	public @Nullable Style getCurrentStyle() {
		return currentStyle;
	}
}
