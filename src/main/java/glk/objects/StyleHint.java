/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects;

public enum StyleHint {
	Indentation {
		@Override
		public String asHTML(int value) {
			return "";
		}
	},
	ParaIndentation {
		@Override
		public String asHTML(int value) {
			return "text-indent: " + Style.DEFAULT_PARA_INDENT_EM + value + "; ";
		}
	},
	Justification {
		@Override
		public String asHTML(int value) {
			return StyleHintJustification.getStyleHintJustification(value).asHTML();
		}
	},
	Size {
		@Override
		public String asHTML(int value) {
			return "font-size: " + Style.DEFAULT_FONT_SIZE + value + "; ";
		}
	},
	Weight {
		@Override
		public String asHTML(int value) {
			return "font-weight: " + switch (value) {
				case 1 -> "bold";
				case 0 -> "normal";
				case -1 -> "lighter";
				default -> throw new IllegalStateException("Unexpected value: " + value);
			} + "; ";
		}
	},
	Oblique {
		@Override
		public String asHTML(int value) {
			return "font-style: " + (value == 1 ? "italic" : "normal") + "; ";
		}
	},
	Proportional {
		@Override
		public String asHTML(int value) {
			return value == 0 ?
					"font-family: 'DejaVu Sans Mono', monospace; " :
					"";
		}
	},
	TextColor {
		@Override
		public String asHTML(int value) {
			return "color: #%1$06x; ".formatted(value);
		}
	},
	BackColor {
		@Override
		public String asHTML(int value) {
			return "background-color: #%1$06x; ".formatted(value);
		}
	},
	ReverseColor {
		@Override
		public String asHTML(int value) {
			return "";
		}
	};

	public static StyleHint getStyleHint(int id) {
		return StyleHint.values()[id];
	}

	public abstract String asHTML(int value);
}
