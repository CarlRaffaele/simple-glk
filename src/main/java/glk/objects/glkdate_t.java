/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects;

import glk.GLK;
import org.jetbrains.annotations.NotNull;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;

public record glkdate_t(int year,
						int month,
						int day,
						int weekday,
						int hour,
						int minute,
						int second,
						int microsec) implements WritableStruct {

	public glkdate_t(@NotNull TemporalAccessor instant) {
		this(instant.get(ChronoField.YEAR),
				instant.get(ChronoField.MONTH_OF_YEAR),
				instant.get(ChronoField.DAY_OF_MONTH),
				instant.get(ChronoField.DAY_OF_WEEK) == 7 ? // GLK says Sunday is 0
						0 :
						instant.get(ChronoField.DAY_OF_WEEK),
				instant.get(ChronoField.HOUR_OF_DAY),
				instant.get(ChronoField.MINUTE_OF_HOUR),
				instant.get(ChronoField.SECOND_OF_MINUTE),
				instant.get(ChronoField.MICRO_OF_SECOND));
	}

	public glkdate_t(int datePtr) {
		this(GLK.memory.getInt(datePtr),
				GLK.memory.getInt(datePtr + 4),
				GLK.memory.getInt(datePtr + 8),
				GLK.memory.getInt(datePtr + 12),
				GLK.memory.getInt(datePtr + 16),
				GLK.memory.getInt(datePtr + 20),
				GLK.memory.getInt(datePtr + 24),
				GLK.memory.getInt(datePtr + 28));
	}

	public ZonedDateTime toZonedDateTime(ZoneId zoneId) {
		// Horribly inefficient since each addition creates a new copy, but it works for normalization
		return ZonedDateTime.of(0, 1, 1, 0, 0, 0, 0, zoneId)
							.plusYears(year)
							.plusMonths(month - 1)
							.plusDays(day - 1)
							.plusHours(hour)
							.plusMinutes(minute)
							.plusSeconds(second)
							.plusNanos(microsec * 1000L);
	}

	@Override
	public void writeUniqueValue(int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push(year);
			GLK.memory.getCurrentCallFrame().push(month);
			GLK.memory.getCurrentCallFrame().push(day);
			GLK.memory.getCurrentCallFrame().push(weekday);
			GLK.memory.getCurrentCallFrame().push(hour);
			GLK.memory.getCurrentCallFrame().push(minute);
			GLK.memory.getCurrentCallFrame().push(second);
			GLK.memory.getCurrentCallFrame().push(microsec);
		} else {
			GLK.memory.put(address, year);
			GLK.memory.put(address + 4, month);
			GLK.memory.put(address + 8, day);
			GLK.memory.put(address + 12, weekday);
			GLK.memory.put(address + 16, hour);
			GLK.memory.put(address + 20, minute);
			GLK.memory.put(address + 24, second);
			GLK.memory.put(address + 28, microsec);
		}
	}
}
