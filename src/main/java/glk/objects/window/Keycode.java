/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import javafx.scene.input.KeyCode;

import java.util.Optional;

enum Keycode {
	UNKNOWN(0xffffffff),
	LEFT(0xfffffffe),
	RIGHT(0xfffffffd),
	UP(0xfffffffc),
	DOWN(0xfffffffb),
	RETURN(0xfffffffa),
	DELETE(0xfffffff9),
	ESCAPE(0xfffffff8),
	TAB(0xfffffff7),
	PAGEUP(0xfffffff6),
	PAGEDOWN(0xfffffff5),
	HOME(0xfffffff4),
	END(0xfffffff3),
	FUNC1(0xffffffef),
	FUNC2(0xffffffee),
	FUNC3(0xffffffed),
	FUNC4(0xffffffec),
	FUNC5(0xffffffeb),
	FUNC6(0xffffffea),
	FUNC7(0xffffffe9),
	FUNC8(0xffffffe8),
	FUNC9(0xffffffe7),
	FUNC10(0xffffffe6),
	FUNC11(0xffffffe5),
	FUNC12(0xffffffe4),
	MAXVAL(28);

	final int asInt;

	Keycode(int i) {
		this.asInt = i;
	}

	static Optional<Keycode> fromID(int ID) {
		return switch (ID) {
			case 0xffffffff -> Optional.of(UNKNOWN);
			case 0xfffffffe -> Optional.of(LEFT);
			case 0xfffffffd -> Optional.of(RIGHT);
			case 0xfffffffc -> Optional.of(UP);
			case 0xfffffffb -> Optional.of(DOWN);
			case 0xfffffffa -> Optional.of(RETURN);
			case 0xfffffff9 -> Optional.of(DELETE);
			case 0xfffffff8 -> Optional.of(ESCAPE);
			case 0xfffffff7 -> Optional.of(TAB);
			case 0xfffffff6 -> Optional.of(PAGEUP);
			case 0xfffffff5 -> Optional.of(PAGEDOWN);
			case 0xfffffff4 -> Optional.of(HOME);
			case 0xfffffff3 -> Optional.of(END);
			case 0xffffffef -> Optional.of(FUNC1);
			case 0xffffffee -> Optional.of(FUNC2);
			case 0xffffffed -> Optional.of(FUNC3);
			case 0xffffffec -> Optional.of(FUNC4);
			case 0xffffffeb -> Optional.of(FUNC5);
			case 0xffffffea -> Optional.of(FUNC6);
			case 0xffffffe9 -> Optional.of(FUNC7);
			case 0xffffffe8 -> Optional.of(FUNC8);
			case 0xffffffe7 -> Optional.of(FUNC9);
			case 0xffffffe6 -> Optional.of(FUNC10);
			case 0xffffffe5 -> Optional.of(FUNC11);
			case 0xffffffe4 -> Optional.of(FUNC12);
			default -> Optional.empty();
		};
	}

	@SuppressWarnings("DuplicatedCode")
	static Optional<Keycode> fromFXKeycode(KeyCode keyCode) {
		return switch (keyCode) {
			case UNDEFINED -> Optional.of(UNKNOWN);
			case LEFT, KP_LEFT -> Optional.of(LEFT);
			case RIGHT, KP_RIGHT -> Optional.of(RIGHT);
			case UP, KP_UP -> Optional.of(UP);
			case DOWN, KP_DOWN -> Optional.of(DOWN);
			case ENTER -> Optional.of(RETURN);
			case DELETE, BACK_SPACE -> Optional.of(DELETE);
			case ESCAPE -> Optional.of(ESCAPE);
			case TAB -> Optional.of(TAB);
			case PAGE_UP -> Optional.of(PAGEUP);
			case PAGE_DOWN -> Optional.of(PAGEDOWN);
			case HOME -> Optional.of(HOME);
			case END -> Optional.of(END);
			case F1 -> Optional.of(FUNC1);
			case F2 -> Optional.of(FUNC2);
			case F3 -> Optional.of(FUNC3);
			case F4 -> Optional.of(FUNC4);
			case F5 -> Optional.of(FUNC5);
			case F6 -> Optional.of(FUNC6);
			case F7 -> Optional.of(FUNC7);
			case F8 -> Optional.of(FUNC8);
			case F9 -> Optional.of(FUNC9);
			case F10 -> Optional.of(FUNC10);
			case F11 -> Optional.of(FUNC11);
			case F12 -> Optional.of(FUNC12);
			default -> Optional.empty();
		};
	}
}
