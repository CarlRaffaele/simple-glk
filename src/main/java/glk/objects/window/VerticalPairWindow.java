/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import javafx.application.Platform;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicBoolean;

public class VerticalPairWindow extends BasePairWindow {

	public VerticalPairWindow(Window keyWindow, Window newWindow, @NotNull MethodDirection direction) {
		super(keyWindow, newWindow, direction);

		VBox vBox = new VBox();
		super.container = vBox;
		vBox.setFillWidth(true);
		w1.prefWidthProperty().bind(vBox.widthProperty());
		w1.setMaxWidth(Double.MAX_VALUE);
		w2.prefWidthProperty().bind(vBox.widthProperty());
		w2.setMaxWidth(Double.MAX_VALUE);
		switch (super.direction) {
			case ABOVE -> Platform.runLater(() -> container.getChildren().addAll(w2.getNode(), w1.getNode()));
			case BELOW -> Platform.runLater(() -> container.getChildren().addAll(w1.getNode(), w2.getNode()));
			default -> throw new IllegalArgumentException("Tried to create a vertical pair with the wrong direction: " + super.direction);
		}
	}

	@Override
	public AtomicBoolean setArrangement(@NotNull MethodDirection direction, @NotNull MethodScaling scaling, @NotNull MethodBorder border, int size, @Nullable Window keyWindow) {
		if (direction != MethodDirection.ABOVE && direction != MethodDirection.BELOW) {
			throw new IllegalArgumentException("Tried to convert a vertical pair into a horizontal");
		}
		setLayoutValues(scaling, border, size, keyWindow, direction);
		Window changeWindow, otherWindow;
		if (direction == this.direction) {
			changeWindow = w2;
			otherWindow = w1;
		} else {
			changeWindow = w1;
			otherWindow = w2;
		}
		AtomicBoolean done = new AtomicBoolean(false);
		Platform.runLater(() -> {
			container.getChildren().clear();
			switch (super.direction) {
				case ABOVE -> container.getChildren().addAll(w2.getNode(), w1.getNode());
				case BELOW -> container.getChildren().addAll(w1.getNode(), w2.getNode());
				default -> throw new IllegalArgumentException("Tried to create a vertical pair with the wrong direction: " + super.direction);
			}
			switch (scaling) {
				case PROPORTIONAL -> {
					changeWindow.setMaxHeight(Double.MAX_VALUE);
					otherWindow.setMaxHeight(Double.MAX_VALUE);
					changeWindow.prefHeightProperty().bind(container.heightProperty().multiply(super.size / 100.0));
					otherWindow.prefHeightProperty()
							   .bind(container.heightProperty().multiply(1 - (super.size / 100.0)));
				}
				case FIXED -> {
					//Clear constraints
					VBox.clearConstraints(changeWindow.getNode());
					VBox.clearConstraints(otherWindow.getNode());
					changeWindow.prefHeightProperty().unbind();
					otherWindow.prefHeightProperty().unbind();
					otherWindow.setMaxHeight(Double.MAX_VALUE);
					//Set new constraints
					changeWindow.setMaxHeight(super.keyWindow.heightUnitsToPixels(size));
					changeWindow.setPrefHeight(changeWindow.getMaxHeight());
					changeWindow.setMinHeight(changeWindow.getMaxHeight());
					otherWindow.setMinHeight(0);
					VBox.setVgrow(changeWindow.getNode(), Priority.NEVER);
					VBox.setVgrow(otherWindow.getNode(), Priority.ALWAYS);
					otherWindow.prefHeightProperty()
							   .bind(container.heightProperty().subtract(changeWindow.getPrefHeight()));
				}
			}
			done.set(true);
		});
		return done;
	}
}