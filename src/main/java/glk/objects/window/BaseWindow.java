/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import blorbReader.Chunk;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gameMemory.MemorySegment;
import glk.GLK;
import glk.objects.EventType;
import glk.objects.event_t;
import glk.objects.stream.Stream;
import glk.objects.stream.WritableStream;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

abstract class BaseWindow implements Window {
	private final int rock;
	private final WindowTypes type;
	protected Stream stream;
	protected boolean hyperlinkRequested = false, mouseEventRequested = false;
	protected boolean echoLineInput = true;
	protected Set<Keycode> lineTerminators;
	private int uniqueValue;
	@Nullable private PairWindow parent;

	BaseWindow(WindowTypes type, int rock) {
		this.type = type;
		this.rock = rock;
		lineTerminators = EnumSet.noneOf(Keycode.class);
	}

	@Override
	public void setParent(@Nullable PairWindow window) {
		parent = window;
	}

	@Override
	public void close(int resultAddress) {
		if (stream != null) {
			stream.close().writeUniqueValue(resultAddress);
		}
		if (this.parent != null && this.parent.getParent().isPresent()) { // Sibling is replacing pair-ent
			((PairWindow) this.parent.getParent().get()).replaceChild(this.parent, this.parent.getOtherChild(this));
		}
	}

	@Override
	public Optional<Window> getSibling() {
		return Optional.ofNullable(parent != null ? parent.getOtherChild(this) : null);
	}

	@Override
	public Optional<Window> getParent() {
		return Optional.ofNullable(parent);
	}

	@Override
	public Optional<WritableStream> getEchoStream() {
		return stream == null ? Optional.empty() : stream.getEcho();
	}

	@Override
	public void setEchoStream(@Nullable WritableStream s) {
		stream.setEcho(s);
	}

	@Override
	public WindowTypes getType() {
		return type;
	}

	@Override
	public Optional<Stream> getStream() {
		return Optional.ofNullable(stream);
	}

	@Override
	public int getRock() {
		return rock;
	}

	@Override
	public void setID(int id) {
		this.uniqueValue = id;
	}

	@Override
	public int hashCode() {
		return uniqueValue;
	}

	@Override
	@SuppressFBWarnings(justification = "False positive", value = {"SA_LOCAL_SELF_COMPARISON"})
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof BaseWindow that)) {
			return false;
		}
		return rock == that.rock &&
				uniqueValue == that.uniqueValue &&
				type == that.type &&
				Objects.equals(stream, that.stream);
	}

	@Override
	public void writeUniqueValue(int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push(uniqueValue);
		} else {
			GLK.memory.put(address, uniqueValue);
		}
	}

	@Override
	public void requestCharEvent() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void requestCharEventUni() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void cancelCharEvent() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void requestLineEvent(int address, int maxLen, int initLen) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void requestLineEventUni(int address, int maxLen, int initLen) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void cancelLineEvent(int eventAddress) {
		new event_t(EventType.LineInput, this, 0, 0).writeUniqueValue(eventAddress);
	}

	@Override
	public void setEchoLineEvent(int val) {
		echoLineInput = val != 0;
	}

	@Override
	public void setLineTerminators(MemorySegment keyCodes) {
		lineTerminators.clear();
		while (keyCodes.hasRemaining()) {
			Optional<Keycode> toAdd = Keycode.fromID(keyCodes.getInt());
			toAdd.ifPresent(lineTerminators::add);
		}
	}

	@Override
	public void requestMouseEvent() {
		mouseEventRequested = true;
	}

	@Override
	public void cancelMouseEvent() {
		mouseEventRequested = false;
	}

	@Override
	public void requestHyperlinkEvent() {
		hyperlinkRequested = true;
	}

	@Override
	public void cancelHyperlinkEvent() {
		hyperlinkRequested = false;
	}

	public class JSInterface {
		private final GLK glk;
		private final Window parent;

		public JSInterface(GLK glk, Window parent) {
			this.glk = glk;
			this.parent = parent;
		}

		//called from JavaScript
		@SuppressWarnings("unused")
		public void hyperlinkClicked(String linkID) {
			if (hyperlinkRequested) {
				glk.insertEvent(new event_t(EventType.Hyperlink, parent, Integer.parseInt(linkID), 0));
			}
		}
	}

	@Override
	public void setCursor(int x, int y) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int heightUnitsToPixels(int height) {
		return height * Window.M_HEIGHT;
	}

	@Override
	public int widthUnitsToPixels(int width) {
		return width * Window.M_WIDTH;
	}

	@Override
	public boolean imageDraw(@NotNull Chunk image, int val1, int val2) {
		return false;
	}

	@Override
	public boolean imageDrawScaled(@NotNull Chunk image, int val1, int val2, int width, int height) {
		return false;
	}

	@Override
	public void insertFlowBreak() {
	}
}
