/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import javafx.application.Platform;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicBoolean;

public class HorizontalPairWindow extends BasePairWindow {
	public HorizontalPairWindow(Window keyWindow, Window newWindow, @NotNull MethodDirection direction) {
		super(keyWindow, newWindow, direction);

		HBox hBox = new HBox();
		super.container = hBox;
		hBox.setFillHeight(true);
		w1.prefHeightProperty().bind(hBox.prefHeightProperty());
		w1.setMaxHeight(Double.MAX_VALUE);
		w2.prefHeightProperty().bind(hBox.prefHeightProperty());
		w2.setMaxHeight(Double.MAX_VALUE);
		switch (super.direction) {
			case LEFT -> Platform.runLater(() -> container.getChildren().addAll(w2.getNode(), w1.getNode()));
			case RIGHT -> Platform.runLater(() -> container.getChildren().addAll(w1.getNode(), w2.getNode()));
			default -> throw new IllegalArgumentException("Tried to create a horizontal pair with the wrong " +
					"direction: " + super.direction);
		}
	}

	@Override
	public AtomicBoolean setArrangement(@NotNull MethodDirection direction, @NotNull MethodScaling scaling, @NotNull MethodBorder border, int size, @Nullable Window keyWindow) {
		if (direction != MethodDirection.LEFT && direction != MethodDirection.RIGHT) {
			throw new IllegalArgumentException("Tried to convert a horizontal pair into a vertical");
		}
		setLayoutValues(scaling, border, size, keyWindow, direction);
		Window changeWindow, otherWindow;
		if (direction == super.direction) {
			changeWindow = w2;
			otherWindow = w1;
		} else {
			changeWindow = w1;
			otherWindow = w2;
		}
		AtomicBoolean done = new AtomicBoolean(false);
		Platform.runLater(() -> {
			container.getChildren().clear();
			switch (super.direction) {
				case LEFT -> container.getChildren().addAll(w2.getNode(), w1.getNode());
				case RIGHT -> container.getChildren().addAll(w1.getNode(), w2.getNode());
				default -> throw new IllegalArgumentException("Tried to create a horizontal pair with the wrong " +
						"direction: " + super.direction);
			}
			switch (scaling) {
				case PROPORTIONAL -> {
					changeWindow.setMaxWidth(Double.MAX_VALUE);
					otherWindow.setMaxWidth(Double.MAX_VALUE);
					changeWindow.prefWidthProperty().bind(container.widthProperty().multiply(super.size / 100.0));
					otherWindow.prefWidthProperty().bind(container.widthProperty().multiply(1 - (super.size / 100.0)));
				}
				case FIXED -> {
					//Clear constraints
					HBox.clearConstraints(changeWindow.getNode());
					HBox.clearConstraints(otherWindow.getNode());
					changeWindow.prefWidthProperty().unbind();
					otherWindow.setMaxWidth(Double.MAX_VALUE);
					//Set new constraints
					changeWindow.setMaxWidth(super.keyWindow.widthUnitsToPixels(size));
					changeWindow.setPrefWidth(changeWindow.getMaxWidth());
					changeWindow.setMinWidth(changeWindow.getMaxWidth());
					otherWindow.setMinWidth(0);
					HBox.setHgrow(changeWindow.getNode(), Priority.NEVER);
					HBox.setHgrow(otherWindow.getNode(), Priority.ALWAYS);
					otherWindow.prefWidthProperty()
							   .bind(container.widthProperty().subtract(changeWindow.getPrefWidth()));
				}
			}
			done.set(true);
		});
		return done;
	}
}
