/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import glk.objects.stream.FileMode;
import glk.objects.stream.Stream;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

class WebBlankWindow extends BaseWindow {
	private final Pane empty;

	WebBlankWindow(int rock) {
		super(WindowTypes.BLANK, rock);
		super.stream = Stream.windowStreamFrom(WindowTypes.BLANK, FileMode.WRITE, rock, null, null, null);
		empty = new Pane();
	}

	@Override
	public void getSize(int address1, int address2) {
		Window.getSize(address1, address2, 0, 0);
	}

	@Override
	public void clear() {

	}

	@Override
	public Node getNode() {
		return null;
	}

	@Override
	public DoubleProperty prefWidthProperty() {
		return empty.prefWidthProperty();
	}

	@Override
	public DoubleProperty prefHeightProperty() {
		return empty.prefHeightProperty();
	}

	@Override
	public void setMaxWidth(double width) {
		empty.setMaxWidth(width);
	}

	@Override
	public void setMaxHeight(double height) {
		empty.setMaxHeight(height);
	}

	@Override
	public double getMaxWidth() {
		return empty.getMaxWidth();
	}

	@Override
	public double getMaxHeight() {
		return empty.getMaxHeight();
	}

	@Override
	public void setPrefWidth(double width) {
		empty.setPrefWidth(width);
	}

	@Override
	public void setPrefHeight(double height) {
		empty.setPrefHeight(height);
	}

	@Override
	public double getPrefWidth() {
		return empty.getPrefWidth();
	}

	@Override
	public double getPrefHeight() {
		return empty.getPrefHeight();
	}

	@Override
	public void setMinHeight(double height) {
		empty.setMinHeight(height);
	}

	@Override
	public void setMinWidth(double width) {
		empty.setMinWidth(width);
	}
}
