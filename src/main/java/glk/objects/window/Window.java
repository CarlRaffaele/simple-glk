/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.window;

import blorbReader.Chunk;
import gameMemory.MemorySegment;
import glk.GLK;
import glk.objects.GLKOpaqueObject;
import glk.objects.Style;
import glk.objects.StyleHint;
import glk.objects.stream.Stream;
import glk.objects.stream.WritableStream;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Optional;

public interface Window extends GLKOpaqueObject {
	int M_HEIGHT = 16;
	int M_WIDTH = 8;
	int NEWLINE = 10; //\n

	default void getSize(int address1, int address2) {
		getSize(address1, address2, 0, 0);
	}

	void setCursor(int x, int y);

	Optional<WritableStream> getEchoStream();

	void setEchoStream(WritableStream s);

	WindowTypes getType();

	void clear();

	Optional<Stream> getStream();

	void setParent(PairWindow window);

	Optional<Window> getSibling();

	Optional<Window> getParent();

	void close(int resultAddress);

	Node getNode();

	default boolean isDoneLoading() {
		return true;
	}

	void requestCharEvent();

	void requestCharEventUni();

	void cancelCharEvent();

	void requestLineEvent(int address, int maxLen, int initLen);

	void requestLineEventUni(int address, int maxLen, int initLen);

	void cancelLineEvent(int eventAddress);

	void setEchoLineEvent(int val);

	void setLineTerminators(MemorySegment keyCodes);

	void requestMouseEvent();

	void cancelMouseEvent();

	void requestHyperlinkEvent();

	void cancelHyperlinkEvent();

	DoubleProperty prefWidthProperty();

	DoubleProperty prefHeightProperty();

	void setMaxWidth(double width);

	void setMaxHeight(double height);

	double getMaxWidth();

	double getMaxHeight();

	void setMinHeight(double height);

	void setMinWidth(double width);

	void setPrefWidth(double width);

	void setPrefHeight(double height);

	double getPrefWidth();

	double getPrefHeight();

	int heightUnitsToPixels(int height);

	int widthUnitsToPixels(int width);

	boolean imageDraw(@NotNull Chunk image, int val1, int val2);

	boolean imageDrawScaled(@NotNull Chunk image, int val1, int val2, int width, int height);

	void insertFlowBreak();

	@NotNull
	@Contract("_, _, _ -> new")
	static PairWindow split(@NotNull Window splitWindow, Window newWindow, MethodDirection direction) {
		Optional<Window> save = splitWindow.getParent(); // PairWindow constructor sets the parent to itself
		PairWindow ret = switch (direction) {
			case ABOVE, BELOW -> new VerticalPairWindow(splitWindow, newWindow, direction);
			case LEFT, RIGHT -> new HorizontalPairWindow(splitWindow, newWindow, direction);
		};
		save.ifPresent(parent -> ((PairWindow) parent).replaceChild(splitWindow, ret));
		return ret;
	}

	@NotNull
	@Contract("_, _, _, _ -> new")
	static Window windowFrom(WindowTypes wintype, int rock, GLK glk, Map<Style, Map<StyleHint, Integer>> styleHints) {
		return switch (wintype) {
			case TEXT_BUFFER -> new WebTextBufferWindow(rock, glk, styleHints);
			case BLANK -> new WebBlankWindow(rock);
			case TEXT_GRID -> new CanvasTextGridWindow(rock, styleHints);
			case GRAPHICS -> new GraphicsWindowImpl(rock, glk);
			case ALL, PAIR -> throw new IllegalArgumentException("Tried to create a window that can't be created: " + wintype);
		};
	}

	static void getSize(int address1, int address2, int width, int height) {
		if (address1 == -1) {
			GLK.memory.getCurrentCallFrame().push(width);
		} else {
			GLK.memory.put(address1, width);
		}
		if (address2 == -1) {
			GLK.memory.getCurrentCallFrame().push(height);
		} else {
			GLK.memory.put(address2, height);
		}
	}
}

