/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.atomic.AtomicBoolean;

public interface PairWindow extends Window {
	Window getOtherChild(@NotNull Window window);

	void replaceChild(@NotNull Window child, Window replacement);

	AtomicBoolean setArrangement(MethodDirection direction, MethodScaling scaling, MethodBorder border, int size,
								 @Nullable Window keyWindow);

	default AtomicBoolean setArrangement(int method, int size, @Nullable Window keyWindow) {
		return setArrangement(MethodDirection.get(method), MethodScaling.get(method), MethodBorder.get(method), size,
				keyWindow);
	}

	void getArrangement(int methodAddr, int sizeAddr, int keyWinAddr);
}
