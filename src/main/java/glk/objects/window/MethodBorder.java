/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import org.jetbrains.annotations.Contract;

public enum MethodBorder implements WindowDisplayMethod {
	BORDER,
	NOBORDER;

	private static final int BORDER_MASK = 0x100;

	@Contract(pure = true)
	public static MethodBorder get(int id) {
		return switch (id & BORDER_MASK) {
			case 0x000 -> BORDER;
			case 0x100 -> NOBORDER;
			default -> throw new IllegalArgumentException("Tried to create a Window scaling of unknown type: " + id);
		};
	}

	@Override
	public int getID() {
		return switch (this) {
			case BORDER -> 0;
			case NOBORDER -> 0x100;
		};
	}
}
