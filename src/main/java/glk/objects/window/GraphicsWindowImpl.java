/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.window;

import blorbReader.Chunk;
import glk.GLK;
import glk.objects.EventType;
import glk.objects.event_t;
import glk.objects.stream.Stream;
import javafx.animation.AnimationTimer;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GraphicsWindowImpl extends BaseWindow implements GraphicsWindow {
	private final Canvas canvas;
	private final GraphicsContext gc;
	private final GLK glk;
	private final Pane wrapper;
	private Color backgroundColor;
	private static final Logger LOG = LogManager.getLogger(GraphicsWindowImpl.class);
	@NotNull private final Queue<PendingDraw> pendingDraws;
	private boolean liveDrawEnabled;
	private final AnimationTimer timer;

	private static class PendingDraw {
		final double left, top, width, height;
		@Nullable final Image image;
		@Nullable final Color color;

		private PendingDraw(double left, double top, double width, double height, @Nullable Image image, @Nullable Color color) {
			this.left = left;
			this.top = top;
			this.width = width;
			this.height = height;
			this.image = image;
			this.color = color;
		}

		//Draw image with default image width and height
		private PendingDraw(double left, double top, @NotNull Image image) {
			this(left, top, -1, -1, image, null);
		}

		//Draw image with specific size
		private PendingDraw(double left, double top, double width, double height, @NotNull Image image) {
			this(left, top, width, height, image, null);
		}

		//Clear to color
		private PendingDraw(@NotNull Color color) {
			this(0, 0, -1, -1, null, color);
		}

		//Draw color over specific box
		private PendingDraw(double left, double top, double width, double height, @NotNull Color color) {
			this(left, top, width, height, null, color);
		}
	}

	GraphicsWindowImpl(int rock, GLK glk) {
		super(WindowTypes.GRAPHICS, rock);
		canvas = new Canvas();
		gc = canvas.getGraphicsContext2D();
		super.stream = Stream.windowStreamFrom(WindowTypes.GRAPHICS, null, rock, null, canvas.getGraphicsContext2D(), null);
		this.glk = glk;
		canvas.setOnMouseClicked(this::mouseClickHandler);
		wrapper = new Pane();
		wrapper.getChildren().add(canvas);
		canvas.widthProperty().bind(wrapper.widthProperty());
		canvas.heightProperty().bind(wrapper.heightProperty());
		pendingDraws = new ConcurrentLinkedQueue<>();
		this.liveDrawEnabled = GLK.isEnableLiveDrawing();
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				draw();
			}
		};
	}

	@Override
	public void draw() {
		while (!pendingDraws.isEmpty()) {
			PendingDraw pd = pendingDraws.remove();
			if (pd.color != null) {
				gc.setFill(pd.color);
				if (pd.width == -1 || pd.height == -1) {
					double w = canvas.getWidth();
					double h = canvas.getHeight();
					gc.fillRect(pd.left, pd.top, w, h);
				} else {
					gc.fillRect(pd.left, pd.top, pd.width, pd.height);
				}
			} else if (pd.image != null) {
				if (pd.width == -1 || pd.height == -1) {
					gc.drawImage(pd.image, pd.left, pd.top);
				} else {
					gc.drawImage(pd.image, pd.left, pd.top, pd.width, pd.height);
				}
			} else {
				LOG.warn("Confused pending draw: " + pd.toString());
			}
		}
	}

	@Override
	public void setID(int id) {
		if (liveDrawEnabled) {
			timer.start();
		}
		super.setID(id);
	}

	@Override
	public void close(int resultAddress) {
		timer.stop();
		super.close(resultAddress);
	}

	@Override
	public void setLiveDraw(boolean enable) {
		liveDrawEnabled = enable;
		if (enable) {
			timer.start();
		} else {
			timer.stop();
		}
	}

	private void mouseClickHandler(@NotNull MouseEvent event) {
		if (super.mouseEventRequested) {
			glk.insertEvent(new event_t(EventType.MouseInput, this, ((int) event.getX()), ((int) event.getY())));
		}
	}

	private static Color colorFromInt(int color) {
		return Color.web(String.format("0x%06x", color));
	}

	@Override
	public void setBackgroundColor(int color) {
		try {
			backgroundColor = colorFromInt(color);
		} catch (Exception e) {
			LOG.warn("Failed to properly format a color: ", e);
		}
	}

	@Override
	public void fillRect(int color, int left, int top, int width, int height) {
		pendingDraws.add(new PendingDraw(left, top, width, height, colorFromInt(color)));
	}

	@Override
	public void eraseRect(int left, int top, int width, int height) {
		pendingDraws.add(new PendingDraw(left, top, width, height, backgroundColor));
	}

	@Override
	public void clear() {
		pendingDraws.add(new PendingDraw(backgroundColor));
	}

	@Override
	public Node getNode() {
		return wrapper;
	}

	@Override
	public boolean isDoneLoading() {
		return true;
	}

	@Override
	public DoubleProperty prefWidthProperty() {
		return wrapper.prefWidthProperty();
	}

	@Override
	public DoubleProperty prefHeightProperty() {
		return wrapper.prefHeightProperty();
	}

	@Override
	public void setMaxWidth(double width) {
		wrapper.setMaxWidth(width);
	}

	@Override
	public void setMaxHeight(double height) {
		wrapper.setMaxHeight(height);
	}

	@Override
	public double getMaxWidth() {
		return wrapper.getMaxWidth();
	}

	@Override
	public double getMaxHeight() {
		return wrapper.getMaxHeight();
	}

	@Override
	public void setPrefWidth(double width) {
		wrapper.setPrefWidth(width);
	}

	@Override
	public void setPrefHeight(double height) {
		wrapper.setPrefHeight(height);
	}

	@Override
	public double getPrefWidth() {
		return wrapper.getPrefWidth();
	}

	@Override
	public double getPrefHeight() {
		return wrapper.getPrefHeight();
	}

	@Override
	public int heightUnitsToPixels(int height) {
		return height;
	}

	@Override
	public int widthUnitsToPixels(int width) {
		return width;
	}

	@Override
	public void getSize(int address1, int address2) {
		Window.getSize(address1, address2, ((int) canvas.getWidth()), ((int) canvas.getHeight()));
	}

	@Override
	public void setMinHeight(double height) {
		wrapper.setMinHeight(height);
	}

	@Override
	public void setMinWidth(double width) {
		wrapper.setMinWidth(width);
	}

	@Override
	public boolean imageDraw(@NotNull Chunk image, int val1, int val2) {
		pendingDraws.add(new PendingDraw(val1, val2, image.getAsImage()));
		return true;
	}

	@Override
	public boolean imageDrawScaled(@NotNull Chunk image, int val1, int val2, int width, int height) {
		pendingDraws.add(new PendingDraw(val1, val2, width, height, image.getAsImage()));
		return true;
	}
}
