/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.window;

import glk.objects.Style;
import glk.objects.StyleHint;
import glk.objects.stream.CursorStream;
import glk.objects.stream.Stream;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class CanvasTextGridWindow extends BaseWindow {
	@NotNull private final Canvas canvas;
	@NotNull private final Pane wrapper;

	CanvasTextGridWindow(int rock, Map<Style, Map<StyleHint, Integer>> styleHints) {
		super(WindowTypes.TEXT_GRID, rock);
		canvas = new Canvas();
		super.stream = Stream.windowStreamFrom(WindowTypes.TEXT_GRID, null, rock, null, canvas.getGraphicsContext2D(),
				styleHints);
		wrapper = new Pane();
		wrapper.getChildren().add(canvas);
		canvas.widthProperty().bind(wrapper.widthProperty());
		canvas.heightProperty().bind(wrapper.heightProperty());
	}

	@Override
	public void setCursor(int x, int y) {
		((CursorStream) stream).setCursorPosition(x, y);
	}

	@Override
	public void clear() {
		stream.clear();
	}

	@Override
	public Node getNode() {
		return wrapper;
	}

	@Override
	public DoubleProperty prefWidthProperty() {
		return wrapper.prefWidthProperty();
	}

	@Override
	public DoubleProperty prefHeightProperty() {
		return wrapper.prefHeightProperty();
	}

	@Override
	public void setMaxWidth(double width) {
		wrapper.setMaxWidth(width);
	}

	@Override
	public void setMaxHeight(double height) {
		wrapper.setMaxHeight(height);
	}

	@Override
	public double getMaxWidth() {
		return wrapper.getMaxWidth();
	}

	@Override
	public double getMaxHeight() {
		return wrapper.getMaxHeight();
	}

	@Override
	public void setPrefWidth(double width) {
		wrapper.setPrefWidth(width);
	}

	@Override
	public void setPrefHeight(double height) {
		wrapper.setPrefHeight(height);
	}

	@Override
	public double getPrefWidth() {
		return wrapper.getPrefWidth();
	}

	@Override
	public double getPrefHeight() {
		return wrapper.getPrefHeight();
	}

	@Override
	public int heightUnitsToPixels(int height) {
		return height * M_HEIGHT;
	}

	@Override
	public int widthUnitsToPixels(int width) {
		return width * M_WIDTH;
	}

	@Override
	public void setMinHeight(double height) {
		wrapper.setMinHeight(height);
	}

	@Override
	public void setMinWidth(double width) {
		wrapper.setMinWidth(width);
	}

	@Override
	public void getSize(int address1, int address2) {
		Window.getSize(address1, address2,
				((int) (canvas.getWidth() / Window.M_WIDTH)),
				((int) (canvas.getHeight() / Window.M_HEIGHT)));
	}
}
