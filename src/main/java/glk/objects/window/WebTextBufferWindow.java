/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects.window;

import blorbReader.Chunk;
import gameMemory.MemorySegment;
import glk.GLK;
import glk.objects.EventType;
import glk.objects.Style;
import glk.objects.StyleHint;
import glk.objects.event_t;
import glk.objects.stream.FileMode;
import glk.objects.stream.Stream;
import glk.objects.stream.StyleableStream;
import glk.objects.stream.WritableStream;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.util.EnumSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

class WebTextBufferWindow extends BaseWindow {
	private WebView webViewNode;
	private boolean doneLoading = false;
	private MemorySegment lineInputBuffer, lineInputBufferUni;
	private boolean lineInputActive = false;
	private boolean echoCurrentLineInput;
	private final Set<Keycode> lineTerminators;
	private final JSInterface hold; //holds a reference to prevent GC
	private final ContextMenu menu;
	//To properly remove the EventHandlers they have to be instance variables instead of functions
	private final EventHandler<KeyEvent> charNoncharInputHandler, charCharInputHandler, charCharInputHandlerUni;
	private final EventHandler<KeyEvent> lineInputHandler, lineInputHandlerUni, lineNoncharInputHandler;

	WebTextBufferWindow(int rock, GLK glk, Map<Style, Map<StyleHint, Integer>> styleHints) {
		super(WindowTypes.TEXT_BUFFER, rock);
		hold = new JSInterface(glk, this);
		lineTerminators = EnumSet.noneOf(Keycode.class);
		MenuItem menuItemStyleHints = new MenuItem("View Style Hints");
		menuItemStyleHints.setOnAction(e -> {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			StringBuilder sb = new StringBuilder();
			StyleableStream ss = ((StyleableStream) stream);
			sb.append("Current style = ")
			  .append(ss.getCurrentStyle())
			  .append("\n");
			ss.getStyleHints().forEach((style, format) -> sb.append(style)
															.append(" = ")
															.append(format)
															.append("\n"));
			alert.setContentText(sb.toString());
			alert.showAndWait();
		});
		menu = new ContextMenu(menuItemStyleHints);
		Platform.runLater(() -> {
			webViewNode = new WebView();
			webViewNode.getEngine().setOnAlert(event -> System.err.println("JS Alert: " + event.getData()));
			webViewNode.getEngine().getLoadWorker().stateProperty().addListener((o, old, newS) -> {
				if (newS == Worker.State.SUCCEEDED) {
					super.stream = Stream.windowStreamFrom(WindowTypes.TEXT_BUFFER, FileMode.WRITE, rock,
							webViewNode.getEngine(), null, styleHints);
					doneLoading = true;
					((JSObject) webViewNode.getEngine().executeScript("window"))
							.setMember("GLKObject", hold);
				}
			});
			webViewNode.getEngine().load(String.valueOf(getClass().getResource("/glk-resources/BaseWebsite.html")));
			webViewNode.getEngine().userStyleSheetLocationProperty().set(String.valueOf(getClass().getResource(
					"/glk-resources/main.css")));

			webViewNode.setContextMenuEnabled(false);
			webViewNode.setOnMouseClicked(event -> {
				if (event.getButton() == MouseButton.SECONDARY) {
					menu.show(webViewNode, event.getScreenX(), event.getScreenY());
				} else {
					menu.hide();
				}
			});
		});

		charNoncharInputHandler = (event) -> {
			KeyCode code = event.getCode();
			if (code.isLetterKey() || code.isDigitKey() || code.isWhitespaceKey()) {
				return;
			}
			Optional<Keycode> kc = Keycode.fromFXKeycode(event.getCode());
			if (kc.isPresent()) { // Translate control codes into the form GLK expects
				glk.insertEvent(new event_t(EventType.CharInput, this, kc.get().asInt, 0));
				event.consume();
				cancelCharEvent();
			}
		};
		charCharInputHandlerUni = (event) -> {
			int codePoint = event.getCharacter().codePointAt(0);
			glk.insertEvent(new event_t(EventType.CharInput, this, codePoint, 0));
			event.consume();
			cancelCharEvent();
		};
		charCharInputHandler = (event) -> {
			byte b = event.getCharacter().getBytes(StandardCharsets.ISO_8859_1)[0];
			glk.insertEvent(new event_t(EventType.CharInput, this, b & 0xff, 0));
			event.consume();
			cancelCharEvent();
		};
		lineNoncharInputHandler = (event) -> {
			KeyCode code = event.getCode();
			if (code.isLetterKey() || code.isDigitKey() || code.isWhitespaceKey()) {
				return;
			}
			Optional<Keycode> kc = Keycode.fromFXKeycode(event.getCode());
			if (kc.isPresent() && lineTerminators.contains(kc.get())) { // Translate control codes into the form GLK expects
				glk.insertEvent(new event_t(EventType.LineInput, this, lineInputBuffer.position(), kc.get().asInt));
				removeLineEvent();
				event.consume();
			}
		};
		lineInputHandler = event -> {
			if (!lineInputActive) {
				return;
			}
			byte b = event.getCharacter().getBytes(StandardCharsets.ISO_8859_1)[0];
			if (b == 13) {
				b = Window.NEWLINE; //replace \r with \n
			}
			if (!lineInputBuffer.hasRemaining() && b != Window.NEWLINE) {
				return;
			}
			if (echoCurrentLineInput) {
				((WritableStream) stream).writeChar(b);
			}
			if (b == Window.NEWLINE) {
				glk.insertEvent(new event_t(EventType.LineInput, this, lineInputBuffer.position(), 0));
				removeLineEvent();
			} else {
				lineInputBuffer.put(b);
			}
			event.consume();
		};
		lineInputHandlerUni = event -> {
			if (!lineInputActive) {
				return;
			}
			int b = event.getCharacter().codePointAt(0);
			if (b == 13) {
				b = Window.NEWLINE; //replace \r with \n
			}
			if (!lineInputBufferUni.hasRemaining() && b != Window.NEWLINE) {
				return;
			}
			if (echoCurrentLineInput) {
				((WritableStream) stream).writeCharUni(b);
			}
			if (b == Window.NEWLINE) {
				glk.insertEvent(new event_t(EventType.LineInput, this, lineInputBufferUni.position(), 0));
				removeLineEvent();
			} else {
				lineInputBufferUni.put(b);
			}
			event.consume();
		};
	}

	@Override
	public void clear() {
		super.stream.clear();
	}

	@Override
	public Node getNode() {
		return webViewNode;
	}

	public boolean isDoneLoading() {
		return doneLoading;
	}

	@Override
	public void requestLineEvent(int address, int maxLen, int initLen) {
		lineInputActive = true;
		lineInputBuffer = GLK.memory.slice(address, maxLen);
		lineInputBuffer.position(initLen);
		echoCurrentLineInput = super.echoLineInput;
		lineTerminators.clear();
		lineTerminators.addAll(super.lineTerminators);
		webViewNode.setOnKeyTyped(lineInputHandler);
		webViewNode.setOnKeyReleased(lineNoncharInputHandler);
	}

	@Override
	public void requestLineEventUni(int address, int maxLen, int initLen) {
		lineInputActive = true;
		lineInputBufferUni = GLK.memory.slice(address, maxLen);
		lineInputBufferUni.position(initLen * 4);
		echoCurrentLineInput = super.echoLineInput;
		lineTerminators.clear();
		lineTerminators.addAll(super.lineTerminators);
		webViewNode.setOnKeyTyped(lineInputHandlerUni);
		webViewNode.setOnKeyReleased(lineNoncharInputHandler);
	}

	private void removeLineEvent() {
		lineInputActive = false;
		lineInputBuffer = null;
		lineInputBufferUni = null;
		webViewNode.removeEventHandler(KeyEvent.KEY_TYPED, lineInputHandler);
		webViewNode.removeEventHandler(KeyEvent.KEY_TYPED, lineInputHandlerUni);
		webViewNode.removeEventHandler(KeyEvent.KEY_RELEASED, lineNoncharInputHandler);
	}

	@Override
	public void cancelLineEvent(int eventAddress) {
		if (lineInputActive) {
			if (eventAddress != 0) {
				if (lineInputBuffer != null) {
					new event_t(EventType.LineInput, this, lineInputBuffer.position(), 0)
							.writeUniqueValue(eventAddress);
				} else if (lineInputBufferUni != null) {
					new event_t(EventType.LineInput, this, lineInputBufferUni.position() / 4, 0)
							.writeUniqueValue(eventAddress);
				} else {
					throw new IllegalStateException("Both line buffers are null during cancelLineEvent");
				}
			}
			removeLineEvent();
		} else {
			if (eventAddress != 0) {
				event_t.NULL_EVENT.writeUniqueValue(eventAddress);
			}
		}
	}

	@Override
	public DoubleProperty prefWidthProperty() {
		return webViewNode.prefWidthProperty();
	}

	@Override
	public DoubleProperty prefHeightProperty() {
		return webViewNode.prefHeightProperty();
	}

	@Override
	public void setMaxWidth(double width) {
		webViewNode.setMaxWidth(width);
	}

	@Override
	public void setMaxHeight(double height) {
		webViewNode.setMaxHeight(height);
	}

	@Override
	public double getMaxWidth() {
		return webViewNode.getMaxWidth();
	}

	@Override
	public double getMaxHeight() {
		return webViewNode.getMaxHeight();
	}

	@Override
	public void setPrefWidth(double width) {
		webViewNode.setPrefWidth(width);
	}

	@Override
	public void setPrefHeight(double height) {
		webViewNode.setMaxHeight(height);
	}

	@Override
	public double getPrefWidth() {
		return webViewNode.getPrefWidth();
	}

	@Override
	public double getPrefHeight() {
		return webViewNode.getPrefHeight();
	}

	@Override
	public void setMinHeight(double height) {
		webViewNode.setMinHeight(height);
	}

	@Override
	public void setMinWidth(double width) {
		webViewNode.setMinWidth(width);
	}

	@Override
	public void requestCharEvent() {
		if (lineInputActive) {
			System.err.println("Warning: activated char and line input on same window");
		}
		webViewNode.addEventHandler(KeyEvent.KEY_PRESSED, charNoncharInputHandler);
		webViewNode.addEventHandler(KeyEvent.KEY_TYPED, charCharInputHandler);
	}

	@Override
	public void requestCharEventUni() {
		if (lineInputActive) {
			System.err.println("Warning: activated char and line input on same window");
		}
		webViewNode.addEventHandler(KeyEvent.KEY_PRESSED, charNoncharInputHandler);
		webViewNode.addEventHandler(KeyEvent.KEY_TYPED, charCharInputHandlerUni);
	}

	@Override
	public void cancelCharEvent() {
		webViewNode.removeEventHandler(KeyEvent.KEY_PRESSED, charNoncharInputHandler);
		webViewNode.removeEventHandler(KeyEvent.KEY_TYPED, charCharInputHandler);
		webViewNode.removeEventHandler(KeyEvent.KEY_TYPED, charCharInputHandlerUni);
	}

	@Override
	public void getSize(int address1, int address2) {
		Window.getSize(address1, address2,
				((int) (webViewNode.getWidth() / Window.M_WIDTH)),
				((int) (webViewNode.getHeight() / Window.M_HEIGHT)));
	}

	@Override
	public boolean imageDraw(@NotNull Chunk image, int val1, int val2) {
		((WritableStream) stream).writeImage(image.getImageBase64(), val1, val2);
		return true;
	}

	@Override
	public boolean imageDrawScaled(@NotNull Chunk image, int val1, int val2, int width, int height) {
		((WritableStream) stream).writeImageScaled(image.getImageBase64(), val1, val2, width, height);
		return true;
	}

	@Override
	public void insertFlowBreak() {
		((WritableStream) stream).insertFlowBreak();
	}
}
