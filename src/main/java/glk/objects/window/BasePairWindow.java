/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects.window;

import glk.GLK;
import glk.objects.stream.WritableStream;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

abstract class BasePairWindow extends BaseWindow implements PairWindow {
	protected MethodDirection direction;
	protected Pane container;
	protected int size;
	protected Window keyWindow, w1, w2;
	protected MethodBorder border;
	protected MethodScaling scaling;
	private static final Logger LOG = LogManager.getLogger(BasePairWindow.class);

	public BasePairWindow(Window keyWindow, Window newWindow, @NotNull MethodDirection direction) {
		super(WindowTypes.PAIR, 0);
		this.w1 = keyWindow;
		this.w2 = newWindow;
		this.keyWindow = newWindow;

		this.w1.setParent(this);
		this.w2.setParent(this);

		this.size = 0;
		this.border = MethodBorder.BORDER;
		this.scaling = MethodScaling.PROPORTIONAL;
		this.direction = direction;
	}

	@Override
	public void close(int resultAddress) {
		w1.close(0);
		w2.close(0);
		super.close(resultAddress);
	}

	@Override
	public void setEchoStream(WritableStream s) {
		// Overridden to prevent an echo stream from being set
		LOG.debug("Tried to set an echo stream on a pair window");
	}

	@Override
	public void clear() {
		// Clearing should do nothing
	}

	@Override
	public Node getNode() {
		return container;
	}

	@Override
	public DoubleProperty prefWidthProperty() {
		return container.prefWidthProperty();
	}

	@Override
	public DoubleProperty prefHeightProperty() {
		return container.prefHeightProperty();
	}

	@Override
	public void setMaxWidth(double width) {
		container.setMaxWidth(width);
	}

	@Override
	public void setMaxHeight(double height) {
		container.setMaxHeight(height);
	}

	@Override
	public double getMaxWidth() {
		return container.getMaxWidth();
	}

	@Override
	public double getMaxHeight() {
		return container.getMaxHeight();
	}

	@Override
	public void setPrefWidth(double width) {
		container.setPrefWidth(width);
	}

	@Override
	public void setPrefHeight(double height) {
		container.setPrefHeight(height);
	}

	@Override
	public double getPrefWidth() {
		return container.getPrefWidth();
	}

	@Override
	public double getPrefHeight() {
		return container.getPrefHeight();
	}

	@Override
	public void setMinHeight(double height) {
		container.setMinHeight(height);
	}

	@Override
	public void setMinWidth(double width) {
		container.setMinWidth(width);
	}

	@Override
	public Window getOtherChild(@NotNull Window window) {
		return window.equals(w1) ? w2 : w1;
	}

	@Override
	public void replaceChild(@NotNull Window child, Window replacement) {
		if (child.equals(w1)) {
			w1 = replacement;
		} else if (child.equals(w2)) {
			w2 = replacement;
		} else {
			throw new IllegalArgumentException("Attempted to replace a child of a pair window that wasn't this pair " +
					"window's child: " + child);
		}
		if (child.equals(this.keyWindow)) {
			keyWindow = null;
		}
		replacement.setParent(this);
		Platform.runLater(() -> {
			container.getChildren().remove(child.getNode());
			container.getChildren().add(replacement.getNode());
		});
		// Re-layout the window
		this.setArrangement(this.direction, this.scaling, this.border, this.size, this.keyWindow);
	}

	@Override
	public void getArrangement(int methodAddr, int sizeAddr, int keyWinAddr) {
		int method = WindowDisplayMethod.calculateBitField(border, direction, scaling);

		writeValue(method, methodAddr);
		writeValue(size, sizeAddr);
		keyWindow.writeUniqueValue(keyWinAddr);
	}

	private static void writeValue(int value, int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push(value);
		} else {
			GLK.memory.put(address, value);
		}
	}

	protected void setLayoutValues(@NotNull MethodScaling scaling, @NotNull MethodBorder border,
								   int size, @Nullable Window keyWindow, MethodDirection direction) {
		this.scaling = scaling;
		this.border = border;
		this.size = size;
		this.direction = direction;
		if (keyWindow != null) {
			this.keyWindow = keyWindow;
		}
		if (this.keyWindow != w1 && this.keyWindow != w2) {
			LOG.warn("!!!Key Window set to a non-child window!!!");
		}
	}
}
