/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects;

import glk.GLK;

import java.util.Date;

public record glktimeval_t(long seconds, int microsec) implements WritableStruct {
	public static glktimeval_t now() {
		Date date = new Date();
		return new glktimeval_t(date.toInstant().getEpochSecond(), date.toInstant().getNano() / 1000);
	}

	public glktimeval_t(int address) {
		this(GLK.memory.getLong(address), GLK.memory.getInt(address + 8));
	}

	@Override
	public void writeUniqueValue(int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push((int) (seconds >> 32));
			GLK.memory.getCurrentCallFrame().push((int) seconds);
			GLK.memory.getCurrentCallFrame().push(microsec);
		} else {
			GLK.memory.put(address, seconds);
			GLK.memory.put(address + 8, microsec);
		}
	}
}
