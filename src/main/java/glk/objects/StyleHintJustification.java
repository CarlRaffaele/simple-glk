/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects;

public enum StyleHintJustification {
	LeftFlush {
		@Override
		public String asHTML() {
			return "text-align: left; ";
		}
	},
	LeftRight {
		@Override
		public String asHTML() {
			return "text-align: justify; ";
		}
	},
	Centered {
		@Override
		public String asHTML() {
			return "text-align: center; ";
		}
	},
	RightFlush {
		@Override
		public String asHTML() {
			return "text-align: right;";
		}
	};

	public static StyleHintJustification getStyleHintJustification(int id) {
		return StyleHintJustification.values()[id];
	}

	public abstract String asHTML();
}
