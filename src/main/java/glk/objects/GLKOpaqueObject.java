/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package glk.objects;

import org.jetbrains.annotations.Contract;

public interface GLKOpaqueObject extends WritableStruct {

	int getRock();

	void setID(int id);

	@Override
	int hashCode();

	@Contract(value = "null -> false", pure = true)
	@Override
	boolean equals(Object o);
}
