/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package glk.objects;

import glk.GLK;
import glk.objects.window.Window;

public record event_t(EventType type, Window win, int val1, int val2) implements WritableStruct {
	public static final event_t NULL_EVENT = new event_t(EventType.None, null, 0, 0);
	public static final event_t TIMER_EVENT = new event_t(EventType.Timer, null, 0, 0);

	@Override
	public void writeUniqueValue(int address) {
		if (address == -1) {
			GLK.memory.getCurrentCallFrame().push(type.ordinal());
			GLK.memory.getCurrentCallFrame().push(win != null ? win.hashCode() : 0);
			GLK.memory.getCurrentCallFrame().push(val1);
			GLK.memory.getCurrentCallFrame().push(val2);
		} else {
			GLK.memory.put(address, type.ordinal());
			GLK.memory.put(address + 4, win != null ? win.hashCode() : 0);
			GLK.memory.put(address + 8, val1);
			GLK.memory.put(address + 12, val2);
		}
	}
}
